package hi.adventofcode;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Day10 {
	
	public static long countValidSkips(int[] adapters, int first, int last) {
		int len = last-first+1;
		switch(len) {
			case 1:
				return 2L;
			case 2:
				return 4L;
			case 3:
				if(adapters[last+1] - adapters[first-1] == 4) return 7L;
				//else fallthrough
			default: 
//				System.out.printf("[%d]=%d => [%d]=%d\n", first, adapters[first], last, adapters[last]);
				long valid = IntStream.range(0, 1<<len)
						.filter(bitMask -> {
//							int mask2 = bitMask;
//							System.out.printf("(%d)", adapters[first-1]);
//							for(int i=0; i<len; i++) {
//								if((mask2&1)==0) System.out.printf(" -> %d", adapters[i+first]);
//								mask2>>=1;
//							}
//							System.out.printf(" -> (%d)", adapters[last+1]);

							int prev = adapters[first-1];
							for(int i=0; i<len; i++) {
								if((bitMask&1)==1) {
									//skip
								} else {
									//don't skip
									if(adapters[i+first] - prev > 3) {
//										System.out.println(" invalid");
										return false;
									}
									prev = adapters[i+first];
								}
								bitMask>>=1;
							}
							if(adapters[last+1] - prev <= 3) {
//								System.out.println(" valid");
								return true;
							} else {
//								System.out.println(" invalid");
								return false;
							}
						})
						.count();
//				System.out.printf(" #%d\n", valid);
				return valid;
		}
	}
	
	public static void main(String[] args) {
		
		long prep = System.nanoTime();
		
		String s = "0\n0\n"+INPUT;
		int[] adapters = s.lines().mapToInt(Integer::parseInt).toArray();
		
		
		long start = System.nanoTime();
		System.out.printf("Prep: %dus\n", (start - prep)/1000L);
		start = System.nanoTime();
		
		int max = adapters[2];
		for(int i=3; i<adapters.length; i++)
			if(adapters[i]>max) max=adapters[i];
		adapters[1] = max+3; //device
		Arrays.sort(adapters);
		
		int[] diffs = new int[4];
		for(int i=0, j=1; j<adapters.length; i=j++)
			diffs[adapters[j]-adapters[i]]++;
		
		System.out.println(diffs[3]*diffs[1]);
		
		long mid = System.nanoTime();
		System.out.printf("Part 1: %dus\n", (mid - start)/1000L);
		mid = System.nanoTime();
		
		int skipable = 0;
		int[] skipIndicies = new int[100];
		for(int i=1; i<adapters.length-1; i++) {
			if(adapters[i+1]-adapters[i-1] <= 3) {
				skipIndicies[skipable++] = i;
			}
		}
		skipIndicies[skipable] = adapters.length;
//		System.out.println(skipable);
		
//		System.out.println(Arrays.toString(adapters));
//		System.out.println(Arrays.toString(Arrays.stream(skipIndicies, 0, skipable).map(i->adapters[i]).toArray()));
		
		int i = 0;
		long validSolutions = 1L;
		while(i<skipable) {
			int j=i+1;
			while(skipIndicies[j]==skipIndicies[j-1]+1) j++;
			validSolutions*=countValidSkips(adapters, skipIndicies[i], skipIndicies[j-1]);
			i=j;
		}
		System.out.println(validSolutions);
//		System.out.println("1511207993344");
		
		long end = System.nanoTime();
		System.out.printf("Part 2: %dus\n", (end - mid)/1000L);
		
	}
	
	public static final String EXAMPLE1 = "16\n" +
"10\n" +
"15\n" +
"5\n" +
"1\n" +
"11\n" +
"7\n" +
"19\n" +
"6\n" +
"12\n" +
"4";
	
	public static final String EXAMPLE2 = "28\n" +
"33\n" +
"18\n" +
"42\n" +
"31\n" +
"14\n" +
"46\n" +
"20\n" +
"48\n" +
"47\n" +
"24\n" +
"23\n" +
"49\n" +
"45\n" +
"19\n" +
"38\n" +
"39\n" +
"11\n" +
"1\n" +
"32\n" +
"25\n" +
"35\n" +
"8\n" +
"17\n" +
"7\n" +
"9\n" +
"4\n" +
"2\n" +
"34\n" +
"10\n" +
"3";
	
	public static final String INPUT = "97\n" +
"62\n" +
"23\n" +
"32\n" +
"51\n" +
"19\n" +
"98\n" +
"26\n" +
"90\n" +
"134\n" +
"73\n" +
"151\n" +
"116\n" +
"76\n" +
"6\n" +
"94\n" +
"113\n" +
"127\n" +
"119\n" +
"44\n" +
"115\n" +
"50\n" +
"143\n" +
"150\n" +
"86\n" +
"91\n" +
"36\n" +
"104\n" +
"131\n" +
"101\n" +
"38\n" +
"66\n" +
"46\n" +
"96\n" +
"54\n" +
"70\n" +
"8\n" +
"30\n" +
"1\n" +
"108\n" +
"69\n" +
"139\n" +
"24\n" +
"29\n" +
"77\n" +
"124\n" +
"107\n" +
"14\n" +
"137\n" +
"16\n" +
"140\n" +
"80\n" +
"68\n" +
"25\n" +
"31\n" +
"59\n" +
"45\n" +
"126\n" +
"148\n" +
"67\n" +
"13\n" +
"125\n" +
"53\n" +
"57\n" +
"41\n" +
"47\n" +
"35\n" +
"145\n" +
"120\n" +
"12\n" +
"37\n" +
"5\n" +
"110\n" +
"138\n" +
"130\n" +
"2\n" +
"63\n" +
"83\n" +
"22\n" +
"79\n" +
"52\n" +
"7\n" +
"95\n" +
"58\n" +
"149\n" +
"123\n" +
"89\n" +
"109\n" +
"15\n" +
"144\n" +
"114\n" +
"9\n" +
"78";

}
