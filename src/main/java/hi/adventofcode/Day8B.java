package hi.adventofcode;

import java.util.Arrays;

public class Day8B {
	
	public static Program.Operation swap(Program.Operation op) {
		switch(op) {
			case JMP: return Program.Operation.NOP;
			case NOP: return Program.Operation.JMP;
			default: return op;
		}
	}
	
	public static void main(String[] args) {
		Program original = new Program(Day8A.INPUT);
		
		int progLen = original.getInstructions().length;
		for(int i=0; i<progLen; i++) {
			Program.Instruction[] copied = Arrays.copyOf(original.getInstructions(), progLen);
			copied[i] = new Program.Instruction(swap(copied[i].getOperation()), copied[i].getOperand());
			Program p = new Program(copied);
			p.resetExecutionCounts();
			Program.MachineState endState = p.runWhile((state, inst) -> inst.getExecutionCount()==0);
			if(endState.getExecutionSate()==Program.ExecutionSate.END_OF_PROGRAM) {
				System.out.println(endState.getAccumulator());
				return;
			}
		}
		System.out.println("Failed");
	}

}
