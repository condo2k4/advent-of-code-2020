package hi.adventofcode;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiPredicate;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;

public class Debugger {
	
	//Debugger
	private final Set<Program.Instruction> breakpoints = Collections.newSetFromMap(new IdentityHashMap<>());
	private final BiPredicate<Program.MachineState, Program.Instruction> runner;
	private final Program program;
	private Program.MachineState state = new Program.MachineState();
	private boolean suspend;
	private ExecutorService executor = Executors.newSingleThreadExecutor((Runnable r) -> {
		Thread t = new Thread(r);
		t.setDaemon(true);
		return t;
	});
	
	//UI
	private static final Executor UI_THREAD = SwingUtilities::invokeLater;
	private static final Font FONT = new Font(Font.MONOSPACED, Font.PLAIN, 10);
	
	private final JFrame frame;
	private final JButton pause, step, run, reset;
	private final JList<Program.Instruction> programSpace;
	private final JPanel stateSpace;
	
	
	public Debugger(Program program) {
		this.program = program;
		this.runner = (state, inst) -> {
			if(suspend) return false;
			synchronized(breakpoints) {
				if(breakpoints.contains(inst)) return false;
			}
			return true;
		};
		
		frame = new JFrame("AoC 2020 Debugger");
		
		JPanel buttons = new JPanel();
		frame.add(buttons, BorderLayout.NORTH);
		
		pause = new JButton("||");
		pause.setToolTipText("Pause");
		pause.addActionListener(e -> this.suspend=true);
		pause.setEnabled(false);
		buttons.add(pause);
		
		step = new JButton(">");
		step.setToolTipText("Step");
		step.addActionListener(e -> step());
		buttons.add(step);
		
		run = new JButton(">>");
		run.setToolTipText("Run");
		run.addActionListener(e -> run());
		buttons.add(run);
		
		reset = new JButton("X");
		reset.setToolTipText("Reset");
		reset.addActionListener(e -> reset());
		buttons.add(reset);
		
		JPanel pane = new JPanel(new GridLayout(1, 2));
		frame.add(pane, BorderLayout.CENTER);
		
		final int maxPCLength = Integer.toString(program.getInstructions().length).length();
		programSpace = new JList<>(program.getInstructions());
		programSpace.setCellRenderer(new ListCellRenderer<Program.Instruction>() {
			final JLabel lbl = new JLabel();
			final Color bg;
			final Icon bp, normal;
			final String format;
			{
				int iconSize = 10;
				
				lbl.setFont(FONT);
				lbl.setBorder(null);
				lbl.setOpaque(true);
				bg = lbl.getBackground();
				
				BufferedImage bpImg = new BufferedImage(iconSize, iconSize, BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = bpImg.createGraphics();
				g.setColor(Color.red);
				g.fillRect(1, 1, iconSize-2, iconSize-2);
				g.dispose();
				bp = new ImageIcon(bpImg);
				normal = new ImageIcon(new BufferedImage(iconSize, iconSize, BufferedImage.TYPE_INT_ARGB));
				
				format = String.format("%%0%dd: %%4s %%+4d", maxPCLength);
			}
			@Override
			public Component getListCellRendererComponent(JList<? extends Program.Instruction> list, Program.Instruction value, int index, boolean isSelected, boolean cellHasFocus) {
				lbl.setText(String.format(format, index, value.getOperation(), value.getOperand()));
				lbl.setIcon(breakpoints.contains(value) ? bp : normal);
				if(state.getExecutionSate()!=Program.ExecutionSate.EXECUTING && state.getProgramCounter()==index) {
					lbl.setBackground(Color.GREEN);
					lbl.setForeground(Color.BLACK);
				} else {
					lbl.setBackground(bg);
					lbl.setForeground(Color.DARK_GRAY);
				}
				return lbl;
			}
		});
		programSpace.addMouseListener(new MouseAdapter() {
			int clickIndex = -1;
			@Override
			public void mouseReleased(MouseEvent e) {
				if(clickIndex != programSpace.locationToIndex(e.getPoint())) return;
				Program.Instruction instruction = program.getInstructions()[clickIndex];
				synchronized(breakpoints) {
					if(breakpoints.contains(instruction))
						breakpoints.remove(instruction);
					else
						breakpoints.add(instruction);
				}
				programSpace.repaint(programSpace.getCellBounds(clickIndex, clickIndex));
			}

			@Override public void mouseExited(MouseEvent e) { clickIndex = -1; }
			@Override public void mousePressed(MouseEvent e) { clickIndex = programSpace.locationToIndex(e.getPoint()); }
		});
		
		pane.add(new JScrollPane(programSpace, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
		
		stateSpace = new JPanel(new BorderLayout());
		stateSpace.add(new JComponent(){
			final String format = String.format("PC: %%-%dd  ACC:%%-10d", maxPCLength);
			@Override protected void paintComponent(Graphics g) {
				String str = String.format(format, state.getProgramCounter(), state.getAccumulator());
				Rectangle2D bounds = g.getFontMetrics().getStringBounds(str, g);
				g.setColor(Color.black);
				g.setFont(FONT);
				g.drawString(str, 3, -(int)bounds.getMinY());
			}
			@Override public Dimension getPreferredSize() { return new Dimension(100, 12); }
		}, BorderLayout.NORTH);
		pane.add(stateSpace);
		
		frame.setSize(500, 500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public void freezeUI() {
		pause.setEnabled(true);
		step.setEnabled(false);
		run.setEnabled(false);
		programSpace.repaint();
		stateSpace.repaint();
	}
	
	public void updateUI() {
		pause.setEnabled(false);
		step.setEnabled(true);
		run.setEnabled(true);
		programSpace.ensureIndexIsVisible(state.getProgramCounter());
		programSpace.repaint();
		stateSpace.repaint();
	}
	
	public void reset() {
		this.suspend=true;
		state = new Program.MachineState();
		updateUI();
	}
	
	public void step() {
		freezeUI();
		program.step(state);
		updateUI();
	}
	
	public void run() {
		freezeUI();
		suspend = false;
		CompletableFuture.supplyAsync(() -> program.runWhile(runner, state), executor)
				.thenAcceptAsync(state -> updateUI(), UI_THREAD);
	}
}
