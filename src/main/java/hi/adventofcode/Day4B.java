package hi.adventofcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 *
 * @author kg249
 */
public class Day4B {
	
	public static void main(String[] args) {
		Map<String, Predicate<String>> rules = new HashMap<>();
		rules.put("byr", val -> { int i = Integer.parseInt(val); return i>=1920 && i<=2002; });
		rules.put("iyr", val -> { int i = Integer.parseInt(val); return i>=2010 && i<=2020; });
		rules.put("eyr", val -> { int i = Integer.parseInt(val); return i>=2020 && i<=2030; });
		rules.put("hcl", Pattern.compile("#[0-9a-f]{6}").asMatchPredicate());
		rules.put("ecl", Pattern.compile("amb|blu|brn|gry|grn|hzl|oth").asMatchPredicate());
		rules.put("pid", Pattern.compile("\\d{9}").asMatchPredicate());
		rules.put("hgt", val -> {
			int i = Integer.parseInt(val.substring(0, val.length()-2));
			if(val.endsWith("cm")) return i>=150 && i<=193;
			return i>=59 && i<=76; //in
		});
		
		int count = 0;
		for(String passport : Day4A.INPUT.split("(\n\\s*){2,}")) {
			if(Arrays.stream(passport.split("\\s+"))
					.filter((String s) -> {
						int i = s.indexOf(':');
						if(i<0) return false;
						try { return rules.getOrDefault(s.substring(0, i), _s->false).test(s.substring(i+1).trim());
						} catch(Throwable t) { return false; }
					})
					.count()==rules.size())
				count++;
		}
		System.out.println(count);
	}

}
