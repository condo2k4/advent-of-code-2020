package hi.adventofcode;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Keith
 */
public class Day15 {

	public static void main(String[] args) {
		int[] starting = INPUT;
		Map<Integer, Integer> memory = new HashMap<>();
		int turn = 1, last = starting[0];
		System.out.printf("1: %d\n", last);
		while(turn < starting.length) {
			memory.put(last, turn);
			last = starting[turn];
			turn++;
			System.out.printf("%d: %d\n", turn, last);
		}
		
		while(turn<2020) {
			int next;
			if(memory.containsKey(last)) {
				next = turn - memory.get(last);
			} else {
				next = 0;
			}
			memory.put(last, turn);
			last = next;
			turn++;
			System.out.printf("%d: %d\n", turn, last);
		}
		
		while(turn<30000000) {
			int next;
			if(memory.containsKey(last)) {
				next = turn - memory.get(last);
			} else {
				next = 0;
			}
			memory.put(last, turn);
			last = next;
			turn++;
			if(turn%1000==0) System.out.printf("%d: %d\n", turn, last);
		}
		
	}
	
	public static final int[] EXAMPLE1 = new int[]{0,3,6}; //436
	public static final int[] EXAMPLE2 = new int[]{1,3,2}; //1
	public static final int[] EXAMPLE3 = new int[]{2,1,3}; //10
	public static final int[] EXAMPLE4 = new int[]{1,2,3}; //27
	public static final int[] EXAMPLE5 = new int[]{2,3,1}; //78
	public static final int[] EXAMPLE6 = new int[]{3,2,1}; //438
	public static final int[] EXAMPLE7 = new int[]{3,1,2}; //1836
	
	public static final int[] INPUT = new int[]{0,1,4,13,15,12,16};
}
