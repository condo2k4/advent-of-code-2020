package hi.adventofcode;

import java.util.Arrays;
import java.util.Set;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 *
 * @author Keith
 */
public class Day16B {
	
	public static class Rule implements IntPredicate {
		final String name;
		final int a, b;
		final int x, y;
		public Rule(String line) {
			int i = line.indexOf(':');
			name = line.substring(0, i);
			int[] vals = Day16A.RULE_SPLIT.splitAsStream(line.substring(i+2)).mapToInt(Integer::parseInt).toArray();
			a = vals[0]; b = vals[1];
			x = vals[2]; y = vals[3];
		}
		@Override public boolean test(int value) { return (value>=a && value<=b) || (value>=x && value<=y); }

		@Override
		public String toString() {
			return name + ": " + a + "-" + b + " or " + x + "-" + y;
		}
	}
	
	public static class Ticket {
		final int[] values;
		public Ticket(String line) {
			this.values = Day16A.TICKET_SPLIT.splitAsStream(line).mapToInt(Integer::parseInt).toArray();
		}
		public boolean isValid(boolean[] validValues) {
			return IntStream.of(values).allMatch(x->validValues[x]);
		}

		@Override
		public String toString() {
			return "Ticket " + Arrays.toString(values);
		}
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		String[] parts = Day16A.INPUT.split("\n\n");
		
		boolean[] valid = new boolean[1000];
		Arrays.fill(valid, false);
		
		Rule[] rules = parts[0]
				.lines()
				.map(Rule::new)
				.toArray(Rule[]::new);
		
		for(Rule rule: rules) {
			Arrays.fill(valid, rule.a, rule.b+1, true);
			Arrays.fill(valid, rule.x, rule.y+1, true);
		}
		
//		for(Rule r : rules) System.out.println(r);
//		System.out.println();
		
		Ticket myTicket = new Ticket(parts[1].split("\n")[1]);
		final int fieldCount = myTicket.values.length;
		
//		System.out.println(myTicket);
//		System.out.println();
		
		Ticket[] validTickets = Stream.concat(
				parts[2].lines()
						.skip(1)
						.map(Ticket::new)
						.filter(t -> t.isValid(valid)),
				Stream.of(myTicket))
				.toArray(Ticket[]::new);
		
//		for(Ticket t : validTickets) System.out.println(t);
//		System.out.println();
		
		Set<Rule>[] potentialFieldRules = IntStream.range(0, fieldCount)
				.mapToObj(field -> Stream.of(validTickets).mapToInt(t->t.values[field]).toArray())
				.map(values -> Arrays.stream(rules).filter(rule -> IntStream.of(values).allMatch(rule)).collect(Collectors.toSet()))
				.toArray(Set[]::new);
		
//		for(int i=0; i<fieldCount; i++)
//			System.out.printf("%2d: %s\n", i, potentialFieldRules[i].stream().map(r->r.name).collect(Collectors.joining(", ")));
//		System.out.println();
		
		for(;;) {
			boolean changed = false;
			for(int i=0; i<fieldCount; i++) {
				if(potentialFieldRules[i].size()==1) {
					Rule singleton = potentialFieldRules[i].iterator().next();
					for(int j=0; j<fieldCount; j++) {
						if(i!=j) {
							if(potentialFieldRules[j].remove(singleton))
								changed = true;
						}
					}
				}
			}
			if(!changed) break;
		}
		
		Rule[] validRules = Stream.of(potentialFieldRules).map(set -> set.iterator().next()).toArray(Rule[]::new);
		
		// un-needed :D
//		int permutations = Stream.of(potentialFieldRules).mapToInt(List::size).reduce(1, (x,y)->x*y);
//		Rule[] validRules = IntStream.range(0, permutations).parallel()
//				.mapToObj(perm -> {
//					Rule[] permRules = new Rule[fieldCount];
//					for(int i=0; i<fieldCount; i++) {
//						permRules[i] = potentialFieldRules[i].get(perm%potentialFieldRules[i].size());
//						perm /= potentialFieldRules[i].size();
//					}
//					return permRules;
//				})
//				.filter(permRules -> {
//					Set<Rule> ruleSet = new HashSet<>();
//					ruleSet.add(permRules[0]);
//					for(int i=1; i<fieldCount; i++) {
//						if(ruleSet.contains(permRules[i])) return false;
//						ruleSet.add(permRules[i]);
//					}
//					return true;
//				})
//				.filter(permRules -> true) //TODO: check combination against tickets
//				.findAny().orElseThrow();
		
//		for(int i=0; i<fieldCount; i++)
//			System.out.printf("%2d: %s\n", i, validRules[i]);
//		System.out.println();
		
		long value = 1L;
		for(int i=0; i<fieldCount; i++) {
			if(validRules[i].name.startsWith("departure"))
			value*=myTicket.values[i];
		}
		
		long end = System.currentTimeMillis();
		System.out.println(value);
		System.out.printf("Runtime:  %dms\n", end-start);
	}

}
