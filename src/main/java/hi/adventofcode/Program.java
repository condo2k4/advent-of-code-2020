package hi.adventofcode;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Program {
	
	public static enum Operation {
		
		ACC {
			@Override public void execute(int operand, MachineState state) {
				state.accumulator+=operand;
				state.programCounter++;
			}
		},
		JMP {
			@Override public void execute(int operand, MachineState state) {
				state.programCounter+=operand;
			}
		},
		NOP {
			@Override public void execute(int operand, MachineState state) {
				state.programCounter++;
			}
		};
		
		public abstract void execute(int operand, MachineState state);
	}
	
	public enum ExecutionSate {
		INITIALISED, EXECUTING, END_OF_PROGRAM, SUSPENDED, TIMEOUT;
	}
	
	public static class MachineState {
		
		private ExecutionSate executionState = ExecutionSate.INITIALISED;
		private int programCounter;
		private long accumulator = 0;

		public ExecutionSate getExecutionSate() { return executionState; }
		public long getAccumulator() { return accumulator; }
		public int getProgramCounter() { return programCounter; }

		@Override
		public String toString() {
			return String.format("@%-4d\tacc=%-7d", programCounter, accumulator);
		}
	}
	
	public static class Instruction {
		
		private static final Pattern PARSER = Pattern.compile(
				Arrays.stream(Operation.values())
						.map(Operation::name)
						.map(String::toLowerCase)
						.collect(Collectors.joining("|", "^(?<operation>", ")\\s+(?<operand>[+-]?\\d+)(?:\\s+#(?<executions>\\d+))?$")));
		
		public static Instruction parse(String line) {
			Matcher matcher = PARSER.matcher(line);
			if(matcher.matches()) {
				Instruction i = new Instruction(Operation.valueOf(matcher.group("operation").toUpperCase()), Integer.parseInt(matcher.group("operand")));
				if(matcher.start("executions")>=0) i.executionCount=Integer.parseInt(matcher.group("executions"));
				return i;
			}
			return new Instruction(Operation.NOP, 0);
		}
		
		private final Operation operation;
		private final int operand;
		private int executionCount = 0;

		public Instruction(Operation operation, int operand) {
			this.operation = operation;
			this.operand = operand;
		}
		public void execute(MachineState state) {
			operation.execute(operand, state);
			executionCount++;
		}

		@Override
		public String toString() {
			return String.format("%3s %+-4d\t#%d", operation, operand, executionCount);
		}

		@Override
		public int hashCode() {
			int hash = 5;
			hash = 17 * hash + Objects.hashCode(this.operation);
			hash = 17 * hash + this.operand;
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(obj == null || getClass() != obj.getClass()) return false;
			final Instruction other = (Instruction)obj;
			return this.operand == other.operand
					&& this.operation == other.operation;
		}

		public Operation getOperation() { return operation; }
		public int getOperand() { return operand; }
		public int getExecutionCount() { return executionCount; }
		public void resetExecutionCount() { executionCount=0; }
	}
	
	private final Instruction[] instructions;
	private long timeout = 0L;
	
	public Program(String code) {
		instructions = code.lines().map(Instruction::parse).toArray(Instruction[]::new);
	}

	public Program(Instruction[] program) {
		this.instructions = program;
	}
	
	private void step0(MachineState state) {
		instructions[state.programCounter].execute(state);
	}
	
	public void step(MachineState state) {
		state.executionState = ExecutionSate.EXECUTING;
		step0(state);
		state.executionState = state.programCounter>=instructions.length
				? ExecutionSate.END_OF_PROGRAM
				: ExecutionSate.SUSPENDED;
	}
	
	public MachineState run() { return run(new MachineState()); }
	public MachineState run(MachineState state) {
		state.executionState = ExecutionSate.EXECUTING;
		initiateTimeout();
		for(;;) {
			if(state.programCounter>=instructions.length) {
				state.executionState = ExecutionSate.END_OF_PROGRAM;
				return state;
			}
			if(Thread.interrupted()) {
				state.executionState = ExecutionSate.TIMEOUT;
				return state;
			}
			step0(state);
		}
	}
	
	public MachineState runWhile(BiPredicate<MachineState, Instruction> condition) { return runWhile(condition, new MachineState()); }
	public MachineState runWhile(BiPredicate<MachineState, Instruction> condition, MachineState state) {
		state.executionState = ExecutionSate.EXECUTING;
		initiateTimeout();
		for(;;) {
			if(state.programCounter>=instructions.length) {
				state.executionState = ExecutionSate.END_OF_PROGRAM;
				return state;
			}
			if(!condition.test(state, instructions[state.programCounter])) {
				state.executionState = ExecutionSate.SUSPENDED;
				return state;
			}
			if(Thread.interrupted()) {
				state.executionState = ExecutionSate.TIMEOUT;
				return state;
			}
			step0(state);
		}
	}
	
	private void initiateTimeout() {
		if(timeout<=0) return;
		final Thread callingThread = Thread.currentThread();
		new Thread(() -> {
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException ex) {}
			callingThread.interrupt();
		}).start();
	}
	
	@Override public String toString() {
		StringBuilder b = new StringBuilder();
		for(Instruction i : instructions) b.append('\n').append(i);
		return b.toString();
	}

	public long getTimeout() { return timeout; }
	public void setTimeout(long timeout) { this.timeout = timeout; }
	public Instruction[] getInstructions() { return instructions; }
	public void resetExecutionCounts() { for(Instruction i : instructions) i.resetExecutionCount(); }
}