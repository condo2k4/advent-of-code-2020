package hi.adventofcode;

public class Day3B {
	
	public static int trees(int dx, int dy) {
		Iterable<String> lines = ()->Day3A.INPUT.lines().iterator();
		int x=0, j=1, trees=0;
		for(String line : lines) {
			if(--j>0) continue;
			else j=dy;
			if(line.charAt(x%31)=='#') trees++;
			x+=dx;
		}
		return trees;
	}
	
	public static void main(String[] args) {
		long a, b, c, d, e;
		System.out.println(a = trees(1,1));
		System.out.println(b = trees(3,1));
		System.out.println(c = trees(5,1));
		System.out.println(d = trees(7,1));
		System.out.println(e = trees(1,2));
		System.out.println(a*b*c*d*e);
	}

}
