package hi.adventofcode;

public class Day12 {
	
	private static class Ship {
		int x, y, a;
		int s, c;
		int wx, wy;
		public Ship() {
			x=y=a=s=0;
			c=1;
			wy=10;
			wx=1;
		}
		public void adjustAngle(int angle) {
			a+=angle;
			
			if(a>=360) a-=360;
			else if(a<0) a+=360;
			
			switch(a) {
				case   0: s= 0; c= 1; break;
				case  90: s= 1; c= 0; break;
				case 180: s= 0; c=-1; break;
				case 270: s=-1; c= 0; break;
				default: throw new IllegalStateException();
			}
		}
	}
	
	public static void main(String[] args) {
		String[] lines = INPUT.lines().toArray(String[]::new);
		Ship s = new Ship();
//		System.out.println("      pos=(   +0,   +0) heading=  +0");
		for(String line : lines) {
			int arg = Integer.parseInt(line.substring(1));
			switch(line.charAt(0)) {
				case 'N':
					s.y += arg;
					break;
				case 'E':
					s.x += arg;
					break;
				case 'S':
					s.y -= arg;
					break;
				case 'W':
					s.x -= arg;
					break;
				case 'F':
					s.x += arg*s.c;
					s.y += arg*s.s;
					break;
				case 'L':
					s.adjustAngle(arg);
					break;
				case 'R':
					s.adjustAngle(-arg);
					break;
			}
//			System.out.printf("%-4s  pos=(%+5d,%+5d) heading=%+4d {s=%d,c=%d}\n", line, s.x, s.y, s.a, s.s, s.c);
		}
		System.out.printf("pos (%d,%d) = %d\n\n", s.x, s.y, Math.abs(s.x)+Math.abs(s.y));
		
		s = new Ship();
		int tmp;
//		System.out.println("      pos=(   +0,   +0) waypoint=( +10,  +1)");
		for(String line : lines) {
			int arg = Integer.parseInt(line.substring(1));
			switch(line.charAt(0)) {
				case 'N':
					s.wy += arg;
					break;
				case 'E':
					s.wx += arg;
					break;
				case 'S':
					s.wy -= arg;
					break;
				case 'W':
					s.wx -= arg;
					break;
				case 'L':
					for(int i=0; i<arg; i+=90) {
						tmp = s.wy;
						s.wy = s.wx;
						s.wx = -tmp;
					}
					break;
				case 'R':
					for(int i=0; i<arg; i+=90) {
						tmp = s.wx;
						s.wx = s.wy;
						s.wy = -tmp;
					}
					break;
				case 'F':
					s.x += arg*s.wx;
					s.y += arg*s.wy;
					break;
			}
//			System.out.printf("%-4s  pos=(%+5d,%+5d) waypoint=(%+4d,%+4d)\n", line, s.x, s.y, s.wx, s.wy);
		}
		System.out.printf("pos (%d,%d) = %d\n\n", s.x, s.y, Math.abs(s.x)+Math.abs(s.y));
	}
	
	public static final String EXAMPLE1 = "F10\n" +
"N3\n" +
"F7\n" +
"R90\n" +
"F11";
	
	public static final String INPUT = "F77\n" +
"E4\n" +
"S2\n" +
"W1\n" +
"L180\n" +
"N4\n" +
"R180\n" +
"S3\n" +
"W5\n" +
"F86\n" +
"L90\n" +
"E1\n" +
"F16\n" +
"R90\n" +
"N1\n" +
"E1\n" +
"F86\n" +
"S1\n" +
"F36\n" +
"E2\n" +
"L180\n" +
"N5\n" +
"F46\n" +
"N1\n" +
"L90\n" +
"F43\n" +
"S5\n" +
"R90\n" +
"F41\n" +
"W5\n" +
"N1\n" +
"F65\n" +
"E4\n" +
"N1\n" +
"W3\n" +
"F92\n" +
"N5\n" +
"F33\n" +
"R90\n" +
"S5\n" +
"L90\n" +
"W1\n" +
"R180\n" +
"L90\n" +
"S5\n" +
"F27\n" +
"R90\n" +
"N4\n" +
"R90\n" +
"F43\n" +
"E5\n" +
"S2\n" +
"F68\n" +
"N5\n" +
"R90\n" +
"F68\n" +
"R180\n" +
"S2\n" +
"E2\n" +
"S3\n" +
"F41\n" +
"L180\n" +
"E3\n" +
"R90\n" +
"F73\n" +
"R90\n" +
"N1\n" +
"L180\n" +
"N3\n" +
"L180\n" +
"W3\n" +
"S1\n" +
"R180\n" +
"N3\n" +
"F26\n" +
"N5\n" +
"F27\n" +
"L90\n" +
"F30\n" +
"R180\n" +
"N4\n" +
"R90\n" +
"E5\n" +
"N1\n" +
"F70\n" +
"E1\n" +
"L90\n" +
"N3\n" +
"F100\n" +
"L90\n" +
"E5\n" +
"L90\n" +
"S2\n" +
"F85\n" +
"W5\n" +
"R90\n" +
"F85\n" +
"E3\n" +
"R90\n" +
"E5\n" +
"F41\n" +
"R180\n" +
"S1\n" +
"L90\n" +
"F93\n" +
"S1\n" +
"F7\n" +
"N3\n" +
"R270\n" +
"W4\n" +
"S1\n" +
"F47\n" +
"R90\n" +
"N2\n" +
"W4\n" +
"F21\n" +
"S1\n" +
"W1\n" +
"F44\n" +
"L180\n" +
"E5\n" +
"N3\n" +
"W4\n" +
"F11\n" +
"L180\n" +
"E2\n" +
"F36\n" +
"W4\n" +
"F34\n" +
"R90\n" +
"F30\n" +
"N1\n" +
"E1\n" +
"F21\n" +
"N4\n" +
"F59\n" +
"E3\n" +
"F33\n" +
"N1\n" +
"L180\n" +
"W1\n" +
"R90\n" +
"E3\n" +
"F84\n" +
"W3\n" +
"R90\n" +
"W1\n" +
"N1\n" +
"R90\n" +
"F27\n" +
"S3\n" +
"L90\n" +
"N4\n" +
"E3\n" +
"F97\n" +
"N3\n" +
"F30\n" +
"W3\n" +
"F77\n" +
"E5\n" +
"F1\n" +
"R90\n" +
"F96\n" +
"E5\n" +
"N5\n" +
"W2\n" +
"L90\n" +
"S1\n" +
"F46\n" +
"N4\n" +
"F41\n" +
"W5\n" +
"L90\n" +
"S5\n" +
"F79\n" +
"R90\n" +
"F32\n" +
"S3\n" +
"R90\n" +
"F5\n" +
"L90\n" +
"E1\n" +
"R180\n" +
"W2\n" +
"N3\n" +
"L90\n" +
"S1\n" +
"R90\n" +
"W1\n" +
"F78\n" +
"W5\n" +
"N2\n" +
"E2\n" +
"R90\n" +
"S4\n" +
"S2\n" +
"E4\n" +
"F59\n" +
"R270\n" +
"W2\n" +
"L180\n" +
"S3\n" +
"R90\n" +
"W2\n" +
"F41\n" +
"N3\n" +
"F21\n" +
"L270\n" +
"F73\n" +
"N3\n" +
"E4\n" +
"L90\n" +
"E3\n" +
"F97\n" +
"E5\n" +
"N4\n" +
"W4\n" +
"F42\n" +
"W5\n" +
"S3\n" +
"R180\n" +
"N1\n" +
"F56\n" +
"E2\n" +
"F23\n" +
"R90\n" +
"F37\n" +
"L90\n" +
"S5\n" +
"W5\n" +
"R270\n" +
"E4\n" +
"F43\n" +
"W4\n" +
"R90\n" +
"E3\n" +
"N2\n" +
"R90\n" +
"S4\n" +
"L90\n" +
"N5\n" +
"F52\n" +
"E3\n" +
"L90\n" +
"F18\n" +
"F89\n" +
"L90\n" +
"W4\n" +
"F18\n" +
"E1\n" +
"L90\n" +
"E2\n" +
"F40\n" +
"F44\n" +
"R90\n" +
"N5\n" +
"R90\n" +
"S1\n" +
"L90\n" +
"F19\n" +
"N5\n" +
"L180\n" +
"N5\n" +
"W3\n" +
"N4\n" +
"F73\n" +
"R90\n" +
"E5\n" +
"R180\n" +
"F86\n" +
"E5\n" +
"S5\n" +
"F71\n" +
"W4\n" +
"F76\n" +
"S2\n" +
"R180\n" +
"S1\n" +
"L90\n" +
"S2\n" +
"F67\n" +
"R90\n" +
"N5\n" +
"E1\n" +
"F100\n" +
"S3\n" +
"W3\n" +
"N5\n" +
"R90\n" +
"F66\n" +
"L90\n" +
"E1\n" +
"L90\n" +
"W1\n" +
"F93\n" +
"S2\n" +
"F62\n" +
"F31\n" +
"L180\n" +
"F20\n" +
"R180\n" +
"F23\n" +
"W3\n" +
"F53\n" +
"W3\n" +
"R90\n" +
"E5\n" +
"R90\n" +
"N3\n" +
"R90\n" +
"E5\n" +
"F11\n" +
"W4\n" +
"R90\n" +
"W2\n" +
"R180\n" +
"E3\n" +
"N4\n" +
"E3\n" +
"F88\n" +
"L90\n" +
"N2\n" +
"W1\n" +
"R90\n" +
"F13\n" +
"N5\n" +
"W4\n" +
"F7\n" +
"S4\n" +
"W3\n" +
"F34\n" +
"E5\n" +
"S3\n" +
"F32\n" +
"F27\n" +
"N4\n" +
"F49\n" +
"S1\n" +
"F86\n" +
"S1\n" +
"F91\n" +
"S3\n" +
"F80\n" +
"R90\n" +
"S5\n" +
"E2\n" +
"L90\n" +
"F30\n" +
"W1\n" +
"R180\n" +
"W3\n" +
"S1\n" +
"R90\n" +
"F78\n" +
"W5\n" +
"N3\n" +
"R90\n" +
"S4\n" +
"F47\n" +
"F55\n" +
"N5\n" +
"L90\n" +
"F86\n" +
"S3\n" +
"E3\n" +
"F45\n" +
"S1\n" +
"F78\n" +
"S3\n" +
"F37\n" +
"E2\n" +
"F14\n" +
"L180\n" +
"E3\n" +
"F49\n" +
"N1\n" +
"L180\n" +
"F42\n" +
"F3\n" +
"N5\n" +
"E5\n" +
"F96\n" +
"S2\n" +
"L90\n" +
"F27\n" +
"E5\n" +
"S3\n" +
"W3\n" +
"S5\n" +
"F73\n" +
"N1\n" +
"W5\n" +
"S4\n" +
"L90\n" +
"W1\n" +
"S3\n" +
"W3\n" +
"R90\n" +
"E3\n" +
"L90\n" +
"F44\n" +
"L90\n" +
"R180\n" +
"F89\n" +
"W3\n" +
"R180\n" +
"F34\n" +
"E1\n" +
"F35\n" +
"N5\n" +
"R90\n" +
"N5\n" +
"F68\n" +
"L90\n" +
"F82\n" +
"S4\n" +
"F36\n" +
"W2\n" +
"S1\n" +
"S3\n" +
"R90\n" +
"N5\n" +
"E5\n" +
"F18\n" +
"R180\n" +
"S1\n" +
"F87\n" +
"R90\n" +
"F34\n" +
"R180\n" +
"N5\n" +
"E4\n" +
"F12\n" +
"E4\n" +
"L90\n" +
"S3\n" +
"E1\n" +
"S5\n" +
"W2\n" +
"F16\n" +
"E2\n" +
"F15\n" +
"N3\n" +
"W1\n" +
"F17\n" +
"S5\n" +
"L180\n" +
"F60\n" +
"N3\n" +
"F75\n" +
"R90\n" +
"F30\n" +
"E4\n" +
"R90\n" +
"F90\n" +
"S2\n" +
"F13\n" +
"E2\n" +
"F3\n" +
"E1\n" +
"F60\n" +
"S5\n" +
"E2\n" +
"F74\n" +
"L90\n" +
"S3\n" +
"W3\n" +
"F57\n" +
"N4\n" +
"E2\n" +
"F33\n" +
"S4\n" +
"F64\n" +
"N5\n" +
"L90\n" +
"F29\n" +
"N5\n" +
"S2\n" +
"R90\n" +
"F46\n" +
"S4\n" +
"E5\n" +
"L90\n" +
"F68\n" +
"W5\n" +
"S2\n" +
"R180\n" +
"F27\n" +
"W3\n" +
"L90\n" +
"S5\n" +
"R90\n" +
"E1\n" +
"R180\n" +
"F100\n" +
"S4\n" +
"W3\n" +
"R90\n" +
"E1\n" +
"S4\n" +
"W5\n" +
"F20\n" +
"W1\n" +
"N1\n" +
"R90\n" +
"E1\n" +
"N4\n" +
"E1\n" +
"F54\n" +
"N3\n" +
"F77\n" +
"L270\n" +
"N1\n" +
"F26\n" +
"N4\n" +
"E5\n" +
"S5\n" +
"N1\n" +
"F98\n" +
"E4\n" +
"F52\n" +
"W1\n" +
"F6\n" +
"R180\n" +
"N1\n" +
"F31\n" +
"W3\n" +
"N2\n" +
"F100\n" +
"L180\n" +
"E3\n" +
"F43\n" +
"R180\n" +
"E4\n" +
"F8\n" +
"L90\n" +
"W3\n" +
"L270\n" +
"N2\n" +
"R90\n" +
"N2\n" +
"E4\n" +
"L90\n" +
"E5\n" +
"E4\n" +
"S3\n" +
"F89\n" +
"L180\n" +
"S3\n" +
"N5\n" +
"R90\n" +
"F53\n" +
"F43\n" +
"R180\n" +
"E5\n" +
"N5\n" +
"F88\n" +
"W1\n" +
"E4\n" +
"L90\n" +
"W2\n" +
"N5\n" +
"F75\n" +
"L90\n" +
"E1\n" +
"S4\n" +
"F65\n" +
"N3\n" +
"W3\n" +
"F88\n" +
"E2\n" +
"S3\n" +
"E2\n" +
"N2\n" +
"R90\n" +
"S2\n" +
"F98\n" +
"N4\n" +
"S2\n" +
"F13\n" +
"R90\n" +
"N3\n" +
"F74\n" +
"R90\n" +
"F56\n" +
"S2\n" +
"E3\n" +
"S4\n" +
"F72\n" +
"N2\n" +
"R90\n" +
"F21\n" +
"E4\n" +
"N4\n" +
"L90\n" +
"F72\n" +
"L90\n" +
"N1\n" +
"N2\n" +
"F61\n" +
"W2\n" +
"L90\n" +
"F28\n" +
"S3\n" +
"W5\n" +
"S5\n" +
"F81\n" +
"S1\n" +
"E5\n" +
"N3\n" +
"F49\n" +
"N1\n" +
"F4\n" +
"N3\n" +
"F78\n" +
"E1\n" +
"F81\n" +
"N3\n" +
"W4\n" +
"F12\n" +
"L90\n" +
"S3\n" +
"E4\n" +
"F2\n" +
"W2\n" +
"R90\n" +
"S1\n" +
"W2\n" +
"F40\n" +
"S1\n" +
"W1\n" +
"W4\n" +
"N4\n" +
"L90\n" +
"N2\n" +
"E1\n" +
"L180\n" +
"N5\n" +
"F30\n" +
"L90\n" +
"N3\n" +
"F84\n" +
"W1\n" +
"F6\n" +
"S3\n" +
"F72\n" +
"N2\n" +
"W4\n" +
"S4\n" +
"W4\n" +
"E5\n" +
"L90\n" +
"L90\n" +
"N4\n" +
"S2\n" +
"F19\n" +
"N1\n" +
"W5\n" +
"N4\n" +
"L90\n" +
"N2\n" +
"F54\n" +
"L90\n" +
"W4\n" +
"F96\n" +
"N4\n" +
"R180\n" +
"S2\n" +
"F53\n" +
"R90\n" +
"S3\n" +
"E5\n" +
"N5\n" +
"L180\n" +
"E5\n" +
"S4\n" +
"L90\n" +
"N5\n" +
"L90\n" +
"E3\n" +
"L90\n" +
"F63\n" +
"S2\n" +
"L90\n" +
"F35\n" +
"N2\n" +
"F52\n" +
"W1\n" +
"L90\n" +
"F94\n" +
"N5\n" +
"E5\n" +
"R270\n" +
"N2\n" +
"R180\n" +
"S2\n" +
"E2\n" +
"N2\n" +
"S3\n" +
"F86\n" +
"N1\n" +
"F54\n" +
"N1\n" +
"L90\n" +
"W2\n" +
"R90\n" +
"E4\n" +
"R90\n" +
"N3\n" +
"R90\n" +
"F30\n" +
"S4\n" +
"F98\n" +
"W2\n" +
"S5\n" +
"R90\n" +
"N5\n" +
"W2\n" +
"S1\n" +
"F36\n" +
"S3\n" +
"R90\n" +
"S1\n" +
"F84\n" +
"S5\n" +
"N5\n" +
"L180\n" +
"F16\n" +
"N1\n" +
"F55\n" +
"L90\n" +
"N4\n" +
"S2\n" +
"S3\n" +
"R90\n" +
"S5\n" +
"W5\n" +
"N1\n" +
"E4\n" +
"S5\n" +
"W1\n" +
"L180\n" +
"F100\n" +
"E4\n" +
"S3\n" +
"E3\n" +
"N3\n" +
"E1\n" +
"L90\n" +
"W4\n" +
"F60\n" +
"W5\n" +
"L270\n" +
"W1\n" +
"S2\n" +
"L90\n" +
"R90\n" +
"F52\n" +
"S1\n" +
"W3\n" +
"N4\n" +
"F30\n" +
"E2\n" +
"F9\n" +
"F87\n" +
"S5\n" +
"R90\n" +
"S4\n" +
"W5\n" +
"R180\n" +
"S3\n" +
"E4\n" +
"S4\n" +
"L90\n" +
"W3\n" +
"F94\n" +
"F85\n" +
"R90\n" +
"E4\n" +
"W2\n" +
"S2\n" +
"L180\n" +
"W4\n" +
"F28\n" +
"E3\n" +
"N5\n" +
"F53";

}
