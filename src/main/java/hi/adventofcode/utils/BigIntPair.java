package hi.adventofcode.utils;

import java.math.BigInteger;
import java.util.Comparator;
import java.util.Objects;

public class BigIntPair {
	
	public static final Comparator<BigIntPair> BY_FIRST  = (x,y) -> x.a.compareTo(y.a);
	public static final Comparator<BigIntPair> BY_SECOND = (x,y) -> x.b.compareTo(y.b);
	
	protected final BigInteger a, b;

	public BigIntPair(long a, long b) {
		this.a = BigInteger.valueOf(a);
		this.b = BigInteger.valueOf(b);
	}

	public BigIntPair(BigInteger a, BigInteger b) {
		this.a = a;
		this.b = b;
	}

	public BigInteger getA() { return a; }
	public BigInteger getB() { return b; }

	@Override
	public String toString() {
		return "BigIntPair{" + "a=" + a + ", b=" + b + '}';
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 53 * hash + Objects.hashCode(this.a);
		hash = 53 * hash + Objects.hashCode(this.b);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null || getClass() != obj.getClass()) return false;
		final IntPair other = (IntPair)obj;
		return this.a.equals(other.a) && this.b.equals(other.b);
	}

}
