package hi.adventofcode.utils;

import java.util.Comparator;

public class IntTriple extends IntPair {
	
	public static final Comparator<IntTriple> BY_THIRD = (x,y) -> Integer.compare(x.c, y.c);
	
	private final int c;

	public IntTriple(int a, int b, int c) {
		super(a, b);
		this.c = c;
	}

	public int getC() { return c; }

	@Override
	public String toString() {
		return "IntTriple{" + "a=" + a + "b=" + b + "c=" + c + '}';
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 97 * hash + this.a;
		hash = 97 * hash + this.b;
		hash = 97 * hash + this.c;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null || getClass() != obj.getClass()) return false;
		final IntTriple other = (IntTriple)obj;
		return this.a == other.a && this.b == other.b && this.c == other.c;
	}

}
