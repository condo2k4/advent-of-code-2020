package hi.adventofcode.utils;

import java.util.Comparator;

public class IntPair {
	
	public static final Comparator<IntPair> BY_FIRST  = (x,y) -> Integer.compare(x.a, y.a);
	public static final Comparator<IntPair> BY_SECOND = (x,y) -> Integer.compare(x.b, y.b);
	
	protected final int a, b;

	public IntPair(int a, int b) {
		this.a = a;
		this.b = b;
	}

	public int getA() { return a; }
	public int getB() { return b; }

	@Override
	public String toString() {
		return "IntPair{" + "a=" + a + ", b=" + b + '}';
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 79 * hash + this.a;
		hash = 79 * hash + this.b;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null || getClass() != obj.getClass()) return false;
		final IntPair other = (IntPair)obj;
		return this.a == other.a && this.b == other.b;
	}

}
