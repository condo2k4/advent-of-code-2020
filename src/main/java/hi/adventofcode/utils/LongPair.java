package hi.adventofcode.utils;

import java.util.Comparator;

public class LongPair {
	
	public static final Comparator<LongPair> BY_FIRST  = (x,y) -> Long.compare(x.a, y.a);
	public static final Comparator<LongPair> BY_SECOND = (x,y) -> Long.compare(x.b, y.b);
	
	protected final long a, b;

	public LongPair(long a, long b) {
		this.a = a;
		this.b = b;
	}

	public long getA() { return a; }
	public long getB() { return b; }

	@Override
	public String toString() {
		return "LongPair{" + "a=" + a + ", b=" + b + '}';
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 89 * hash + (int)(this.a ^ (this.a >>> 32));
		hash = 89 * hash + (int)(this.b ^ (this.b >>> 32));
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null || getClass() != obj.getClass()) return false;
		final IntPair other = (IntPair)obj;
		return this.a == other.a && this.b == other.b;
	}

}
