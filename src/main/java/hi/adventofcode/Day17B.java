package hi.adventofcode;

/**
 *
 * @author Keith
 */
public class Day17B {
	
	public static final int CYCLES = 6;
	public static final int BUFFER = CYCLES+1;
	
	public static void printStates(boolean[][][][] states, int cycle) {
		int maxX = states.length-1;
		int maxY = states[0].length-1;
		
		switch(cycle) {
			case 0:
				System.out.println("Before any cycles:");
				break;
			case 1:
				System.out.printf("\nAfter 1 cycle:\n\n");
				break;
			default:
				System.out.printf("\nAfter %d cycles:\n\n", cycle);
				break;
		}
		for(int w=BUFFER-cycle; w<=BUFFER+cycle; w++) {
			for(int z=BUFFER-cycle; z<=BUFFER+cycle; z++) {
				System.out.printf("z=%d, w=%d\n", z-BUFFER, w-BUFFER);
				for(int y=BUFFER-cycle; y<=maxY-BUFFER+cycle; y++) {
					for(int x=BUFFER-cycle; x<=maxX-BUFFER+cycle; x++) {
						System.out.print(states[x][y][z][w]?'#':'.');
					}
					System.out.println();
				}
				System.out.println();
			}
		}
	}
	
	public static void main(String[] args) {
		String[] lines = Day17A.INPUT.split("\n");
		boolean[][][][] states = new boolean[(2*BUFFER)+lines[0].length()][(2*BUFFER)+lines.length][(2*BUFFER)+1][(2*BUFFER)+1];
		boolean[][][][] nextStates = new boolean[(2*BUFFER)+lines[0].length()][(2*BUFFER)+lines.length][(2*BUFFER)+1][(2*BUFFER)+1];
		for(int y=0; y<lines.length; y++) {
			for(int x=0; x<lines[0].length(); x++) {
				if(lines[y].charAt(x)=='#')
					states[x+BUFFER][y+BUFFER][BUFFER][BUFFER] = true;
			}
		}
		
//		printStates(states, 0);
		
		int maxX = states.length-1;
		int maxY = states[0].length-1;
		
		for(int cycle=1; cycle<=CYCLES; cycle++) {
			for(int x=1; x<maxX; x++) for(int y=1; y<maxY; y++) for(int z=1; z<(2*BUFFER); z++) for(int w=1; w<(2*BUFFER); w++) {
				int count = 0;
				for(int dx=-1; dx<=1; dx++) for(int dy=-1; dy<=1; dy++) for(int dz=-1; dz<=1; dz++) for(int dw=-1; dw<=1; dw++) {
					if(states[x+dx][y+dy][z+dz][w+dw]) count++;
				}
				nextStates[x][y][z][w] = states[x][y][z][w] ? (count==3 || count==4) : count==3;
			}
			boolean[][][][] tmp = states;
			states = nextStates;
			nextStates = tmp;
			
//			printStates(states, cycle);
		}
		int count = 0;
		for(int x=0; x<=maxX; x++) for(int y=0; y<=maxY; y++) for(int z=0; z<=(2*BUFFER); z++) for(int w=0; w<=(2*BUFFER); w++)
			if(states[x][y][z][w]) count++;
		System.out.println(count);
	}

}
