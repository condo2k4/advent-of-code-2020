package hi.adventofcode;

import hi.adventofcode.utils.BigIntPair;

import java.math.BigInteger;

public class Day13B {
	
	public static BigIntPair bezout(BigInteger p, BigInteger q) {
		if (BigInteger.ZERO.equals(q)) return new BigIntPair(BigInteger.ONE, BigInteger.ZERO);
		BigIntPair vals = bezout(q, p.divideAndRemainder(q)[1]);
		BigInteger a = vals.getB();
		BigInteger b = vals.getA().subtract(p.divide(q).multiply(vals.getB()));
		return new BigIntPair(a, b);
	}
	
	public static BigIntPair chineseRemainderTheorem(BigIntPair a, BigIntPair b) {
		BigInteger a1 = a.getA();
		BigInteger n1 = a.getB();
		BigInteger a2 = b.getA();
		BigInteger n2 = b.getB();
		
//		m1*n1 + m2*n2 = 1
		BigIntPair vals = bezout(n1,n2);
		BigInteger m1 = vals.getA();
		BigInteger m2 = vals.getB();
//		System.out.printf("m1=%d, m2=%d\n", m1, m2);

		BigInteger denom = n1.multiply(n2);
//		System.out.printf("x = %d*%d*%d + %d*%d*%d (mod %d)\n", a1,m2,n2, a2,m1,n1, denom);
		BigInteger x = a1.multiply(m2).multiply(n2).add(a2.multiply(m1).multiply(n1)).mod(denom);
//		System.out.printf("x = %d (mod %d)\n", x, mod);

		return new BigIntPair(x, denom);
	}
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		String[] lines = INPUT.split("\n");
		String[] parts = lines[1].split(",");
		BigIntPair result = new BigIntPair(BigInteger.ZERO, new BigInteger(parts[0]));
		for(int i=1; i<parts.length; i++) {
			if(parts[i].equals("x")) continue;
			BigInteger id = new BigInteger(parts[i]);
			result = chineseRemainderTheorem(result, new BigIntPair(id.subtract(BigInteger.valueOf(i)), id));
		}
		
		long end = System.currentTimeMillis();
		System.out.println("Computed: "+result.getA());
		System.out.println("Expected: "+lines[0]);
		System.out.printf("Runtime:  %dms\n", end-start);
	}
	
	//On each example, first line set to part2 answer
	
	public static final String CUSTOM_EXAMPLE = "102\n" +
"17,x,13";
	
	public static final String EXAMPLE1 = "1068788\n" +
"7,13,x,x,59,x,31,19";
	
	public static final String EXAMPLE2 = "3417\n" +
"17,x,13,19";
	
	public static final String EXAMPLE3 = "754018\n" +
"67,7,59,61";
	
	public static final String EXAMPLE4 = "779210\n" +
"67,x,7,59,61";
	
	public static final String EXAMPLE5 = "1261476\n" +
"67,7,x,59,61";
	
	public static final String EXAMPLE6 = "1202161486\n" +
"1789,37,47,1889";
	
	public static final String INPUT = "1001569619313439\n" +
"19,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,743,x,x,x,x,x,x,x,x,x,x,x,x,13,17,x,x,x,x,x,x,x,x,x,x,x,x,x,x,29,x,643,x,x,x,x,x,37,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,23";

}
