package hi.adventofcode;

/**
 *
 * @author kg249
 */
public class Day1B {
	
	public static void main(String[] args) {
		for(int i : Day1A.input) {
			for(int j : Day1A.input) {
				for(int k : Day1A.input) {
					if(i+j+k==2020) {
						System.out.println(i*j*k);
						return;
					}
				}
			}
		}
	}
	
}
