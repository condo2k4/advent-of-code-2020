package hi.adventofcode;

import java.util.Arrays;
import java.util.regex.Pattern;

/**
 *
 * @author kg249
 */
public class Day4v2 {
	
	private static final String [] PASSPORTS = Day4A.INPUT.split("\n\n");
	
	public static long countValid(String ... regexRules) {
		Pattern[] rules = Arrays.stream(regexRules)
				.map(Pattern::compile)
				.toArray(Pattern[]::new);
		return Arrays.stream(PASSPORTS)
				.filter(s -> Arrays.stream(rules).allMatch(p -> p.matcher(s).find()))
				.count();
	}
	
	public static void main(String[] args) {
		System.out.println(countValid("byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"));
		System.out.println(countValid(
				"byr:(19[2-8]\\d|199\\d|200[0-2])(\\s|$)",
				"iyr:(201\\d|2020)(\\s|$)",
				"eyr:(202\\d|2030)(\\s|$)",
				"hgt:((1[5-8]\\d|19[0-3])cm|(59|6\\d|7[0-6])in)(\\s|$)",
				"hcl:(#[\\da-f]{6})(\\s|$)",
				"ecl:(amb|blu|brn|gry|grn|hzl|oth)(\\s|$)",
				"pid:(\\d{9})(\\s|$)"
		));
	}
	
}