package hi.adventofcode;

import hi.adventofcode.utils.IntPair;

import java.util.stream.Stream;

public class Day13A {
	
	public static void main(String[] args) {
		String[] split = INPUT.split("\n");
		
		final int time = Integer.parseInt(split[0]);
		IntPair next = Stream.of(split[1].split(","))
				.filter(bus->!bus.equals("x"))
				.mapToInt(Integer::parseInt)
				.mapToObj(bus -> new IntPair(bus, (time/bus+1)*bus))
//				.peek(System.out::println)
				.min(IntPair.BY_SECOND)
				.get();
		System.out.println(next.getA()*(next.getB()-time));
	}
	
	public static final String INPUT = "1015292\n" +
"19,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,743,x,x,x,x,x,x,x,x,x,x,x,x,13,17,x,x,x,x,x,x,x,x,x,x,x,x,x,x,29,x,643,x,x,x,x,x,37,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,23";

}
