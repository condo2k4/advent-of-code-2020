package hi.adventofcode;

import java.util.Deque;
import java.util.LinkedList;
import java.util.PrimitiveIterator;

/**
 *
 * @author Keith
 */
public class Day18B {
	
	public static long evaluate(String str) {
		return evaluateExpression(str.chars().filter(c->c!=' ').iterator());
	}
	
	public static long evaluateValue(PrimitiveIterator.OfInt chars) {
		if(!chars.hasNext()) throw new IllegalArgumentException();
		char c = (char)chars.nextInt();
		if(c=='(') return evaluateExpression(chars);
		return (long)(c-'0');
	}
	
	public static long evaluateExpression(PrimitiveIterator.OfInt chars) {
		Deque<Long> stack = new LinkedList<>();
		stack.add(evaluateValue(chars));
loop:	while(chars.hasNext()) {
			switch((char)chars.nextInt()) {
				case '*': stack.addLast(evaluateValue(chars)); break;
				case '+': stack.addLast(Day18A.ADDITION.applyAsLong(stack.pollLast(), evaluateValue(chars))); break;
				case ')': break loop;
				default: throw new IllegalStateException();
			}
		}
		return stack.stream().mapToLong(v->v).reduce(Day18A.MULTIPLY).getAsLong();
	}
	
	public static void main(String[] args) {
		System.out.println(Day18A.INPUT.lines().mapToLong(Day18B::evaluate).sum());
	}

}
