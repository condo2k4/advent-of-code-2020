package hi.adventofcode;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day7 {
	
	public static final Pattern BAG = Pattern.compile("(\\d+) (.+?) bag");
	
	public static final Map<String, Map<String, Integer>> buildTree() {
		return INPUT.lines().collect(Collectors.toMap(
			line -> line.substring(0, line.indexOf("bags contain")-1),
			line -> BAG.matcher(line).results().collect(Collectors.toMap(
					match -> match.group(2),
					match -> Integer.parseInt(match.group(1))))
		));
	}
	
	public static long containedGoldCount(Map<String, Map<String, Integer>> tree) {
		Map<String, Boolean> cache = new HashMap<>();
		return tree.keySet().stream().filter(b -> hasGold(b, tree, cache)).count();
	}
	
	public static boolean hasGold(String bag, Map<String, Map<String, Integer>> tree, Map<String, Boolean> cache) {
		if(cache.containsKey(bag)) return cache.get(bag);
		boolean hasGold = tree.getOrDefault(bag, Collections.emptyMap()).keySet().stream()
				.anyMatch(contained -> contained.equals("shiny gold") || hasGold(contained, tree, cache));
		cache.put(bag, hasGold);
		return hasGold;
	}
	
	public static int getContainedCount(String bag, Map<String, Map<String, Integer>> tree, Map<String, Integer> cache) {
		if(cache.containsKey(bag)) return cache.get(bag);
		int count = tree.getOrDefault(bag, Collections.emptyMap()).entrySet().stream()
				.mapToInt(e -> (getContainedCount(e.getKey(), tree, cache)+1) * e.getValue())
				.sum();
		cache.put(bag, count);
		return count;
	}
	
	public static void main(String[] args) {
		Map<String, Map<String, Integer>> tree = buildTree();
		System.out.println(containedGoldCount(tree));
		System.out.println(Day7.getContainedCount("shiny gold", tree, new HashMap<>()));
	}
	
	public static final String EXAMPLE2 = "shiny gold bags contain 2 dark red bags.\n" +
"dark red bags contain 2 dark orange bags.\n" +
"dark orange bags contain 2 dark yellow bags.\n" +
"dark yellow bags contain 2 dark green bags.\n" +
"dark green bags contain 2 dark blue bags.\n" +
"dark blue bags contain 2 dark violet bags.\n" +
"dark violet bags contain no other bags.";
	
	public static final String INPUT = "striped green bags contain 5 posh indigo bags.\n" +
"light yellow bags contain 3 wavy turquoise bags.\n" +
"bright lime bags contain 2 striped crimson bags, 3 dull red bags.\n" +
"dull blue bags contain 4 posh coral bags, 3 mirrored coral bags, 2 striped fuchsia bags.\n" +
"vibrant coral bags contain 2 shiny blue bags, 2 muted gray bags.\n" +
"mirrored gold bags contain 2 dotted maroon bags.\n" +
"drab lavender bags contain 4 pale turquoise bags, 5 faded lime bags, 2 bright aqua bags.\n" +
"mirrored red bags contain 4 shiny tan bags, 4 muted aqua bags, 4 pale salmon bags, 5 bright violet bags.\n" +
"clear gray bags contain 4 bright lavender bags, 2 dotted plum bags, 1 drab coral bag, 3 faded aqua bags.\n" +
"dull aqua bags contain 2 wavy coral bags.\n" +
"muted yellow bags contain 3 drab olive bags, 4 pale lime bags, 2 striped crimson bags, 3 wavy blue bags.\n" +
"shiny chartreuse bags contain 5 bright yellow bags.\n" +
"posh turquoise bags contain 3 dotted blue bags, 4 pale lime bags, 1 mirrored fuchsia bag.\n" +
"faded maroon bags contain 5 striped indigo bags, 2 light aqua bags, 3 dim chartreuse bags, 4 vibrant tomato bags.\n" +
"dotted lavender bags contain 3 pale crimson bags, 3 wavy gray bags, 2 plaid plum bags, 5 mirrored bronze bags.\n" +
"striped olive bags contain 5 dull lime bags.\n" +
"vibrant aqua bags contain 3 drab tan bags, 5 bright coral bags, 3 pale brown bags.\n" +
"wavy aqua bags contain 4 dim lime bags, 4 dotted bronze bags, 1 bright gray bag.\n" +
"pale green bags contain 1 shiny purple bag, 4 dim plum bags.\n" +
"vibrant purple bags contain 2 pale gray bags, 2 dull crimson bags.\n" +
"faded green bags contain 4 dotted plum bags, 1 light white bag.\n" +
"posh orange bags contain 4 light blue bags.\n" +
"drab tomato bags contain 1 dull salmon bag, 3 plaid orange bags, 3 posh gray bags.\n" +
"vibrant beige bags contain 2 dull fuchsia bags.\n" +
"wavy orange bags contain 2 bright tan bags, 5 light beige bags, 2 vibrant turquoise bags.\n" +
"light salmon bags contain 1 dark blue bag, 2 bright orange bags.\n" +
"light blue bags contain 3 clear chartreuse bags, 5 dull olive bags.\n" +
"dull crimson bags contain 4 dim gold bags, 5 dark tan bags, 5 dark lavender bags.\n" +
"dim teal bags contain 3 striped gray bags, 4 shiny fuchsia bags, 2 vibrant chartreuse bags, 2 drab plum bags.\n" +
"striped crimson bags contain no other bags.\n" +
"dark brown bags contain 1 dim plum bag.\n" +
"dotted tan bags contain 1 striped fuchsia bag.\n" +
"bright turquoise bags contain 3 light orange bags, 2 wavy bronze bags, 2 light beige bags.\n" +
"drab salmon bags contain 4 wavy silver bags.\n" +
"light brown bags contain 1 clear gold bag.\n" +
"plaid aqua bags contain 4 shiny maroon bags, 2 mirrored gray bags.\n" +
"posh chartreuse bags contain 3 clear maroon bags, 4 wavy black bags, 3 vibrant white bags.\n" +
"wavy gray bags contain 3 shiny violet bags, 2 striped silver bags, 5 pale lime bags.\n" +
"dim crimson bags contain 1 wavy blue bag, 1 dull red bag, 1 dotted plum bag.\n" +
"wavy black bags contain 5 dim crimson bags, 5 dotted chartreuse bags.\n" +
"faded gray bags contain 4 plaid white bags, 2 mirrored yellow bags.\n" +
"muted violet bags contain 5 vibrant blue bags, 5 shiny beige bags.\n" +
"dull white bags contain 1 dull olive bag, 5 clear chartreuse bags, 1 dull red bag, 1 wavy red bag.\n" +
"light bronze bags contain 4 bright gray bags, 3 light tomato bags, 2 shiny white bags, 2 clear silver bags.\n" +
"plaid violet bags contain 2 vibrant indigo bags, 2 dim turquoise bags.\n" +
"posh purple bags contain 5 wavy purple bags.\n" +
"bright olive bags contain 4 muted bronze bags, 4 mirrored green bags, 2 dim plum bags, 5 bright silver bags.\n" +
"vibrant olive bags contain 1 vibrant beige bag, 4 striped gold bags, 4 shiny beige bags, 1 faded blue bag.\n" +
"muted fuchsia bags contain 3 vibrant red bags.\n" +
"plaid indigo bags contain 4 dull lime bags, 3 faded violet bags.\n" +
"wavy chartreuse bags contain 3 dotted yellow bags.\n" +
"posh magenta bags contain 4 bright gray bags, 4 dotted turquoise bags.\n" +
"drab teal bags contain 5 dotted plum bags, 2 light white bags, 2 dark plum bags, 4 dim crimson bags.\n" +
"dark coral bags contain 3 wavy chartreuse bags, 3 plaid bronze bags, 1 muted magenta bag.\n" +
"posh white bags contain 4 dotted turquoise bags, 1 mirrored lime bag.\n" +
"mirrored brown bags contain 2 shiny chartreuse bags, 1 light bronze bag, 4 bright bronze bags, 5 dotted crimson bags.\n" +
"faded cyan bags contain 2 dull crimson bags, 4 light chartreuse bags, 4 light salmon bags.\n" +
"bright maroon bags contain 3 dotted yellow bags, 1 bright gray bag, 2 dim yellow bags, 5 light fuchsia bags.\n" +
"clear silver bags contain 4 faded green bags, 4 posh white bags, 4 dark plum bags.\n" +
"shiny crimson bags contain 4 dark lavender bags, 3 dotted gold bags, 3 dark tan bags.\n" +
"wavy olive bags contain 4 dotted cyan bags.\n" +
"wavy coral bags contain 3 dim gold bags, 4 dim silver bags, 4 faded green bags, 2 muted purple bags.\n" +
"striped chartreuse bags contain 4 mirrored plum bags, 4 dull tan bags, 5 muted gold bags.\n" +
"bright blue bags contain 5 mirrored beige bags, 4 muted bronze bags, 1 dark tan bag, 5 dark cyan bags.\n" +
"vibrant red bags contain 3 light chartreuse bags, 2 faded indigo bags, 3 drab teal bags, 1 striped indigo bag.\n" +
"dim cyan bags contain 5 faded silver bags, 3 wavy coral bags.\n" +
"muted gold bags contain 4 pale coral bags.\n" +
"wavy violet bags contain 2 dotted gray bags, 1 clear tan bag, 5 plaid brown bags.\n" +
"light maroon bags contain 5 faded silver bags.\n" +
"light cyan bags contain 5 pale brown bags, 3 pale black bags, 3 light tomato bags, 2 faded violet bags.\n" +
"pale turquoise bags contain 1 dull white bag, 1 muted tomato bag, 2 clear gold bags.\n" +
"clear plum bags contain 2 dotted orange bags, 1 bright olive bag, 3 striped violet bags.\n" +
"plaid silver bags contain 5 muted olive bags, 1 vibrant bronze bag, 5 dull lavender bags, 3 dark purple bags.\n" +
"clear gold bags contain 1 dotted turquoise bag, 3 dark indigo bags.\n" +
"muted indigo bags contain 1 mirrored orange bag, 3 dull beige bags.\n" +
"light plum bags contain 3 drab brown bags, 4 wavy cyan bags, 3 vibrant lavender bags.\n" +
"dim maroon bags contain 4 dull tan bags.\n" +
"pale lime bags contain 4 dotted maroon bags, 2 faded silver bags, 5 shiny tomato bags.\n" +
"shiny black bags contain 2 dull crimson bags.\n" +
"wavy indigo bags contain 1 light fuchsia bag.\n" +
"dull lime bags contain no other bags.\n" +
"shiny fuchsia bags contain 2 drab fuchsia bags, 1 shiny cyan bag, 1 muted black bag, 2 dull aqua bags.\n" +
"muted lavender bags contain 4 dull fuchsia bags, 1 dim chartreuse bag, 1 vibrant gold bag, 2 shiny beige bags.\n" +
"bright cyan bags contain 5 dark gold bags, 2 vibrant cyan bags, 1 pale coral bag.\n" +
"posh black bags contain 3 dull violet bags, 5 drab coral bags.\n" +
"posh olive bags contain 3 muted magenta bags, 3 dark plum bags, 1 shiny violet bag.\n" +
"dull coral bags contain 1 dark gold bag, 2 striped silver bags, 5 vibrant lavender bags, 2 light red bags.\n" +
"clear bronze bags contain 4 striped cyan bags, 4 dotted chartreuse bags, 4 shiny maroon bags.\n" +
"bright red bags contain 2 dark violet bags, 4 pale brown bags.\n" +
"muted silver bags contain 3 bright blue bags, 1 shiny blue bag, 5 muted bronze bags, 1 bright tan bag.\n" +
"dotted maroon bags contain 5 muted white bags, 4 dull lime bags, 2 dim gold bags, 2 faded fuchsia bags.\n" +
"clear lime bags contain 4 striped gold bags, 2 vibrant purple bags, 5 dotted green bags, 2 light tomato bags.\n" +
"shiny tan bags contain 5 dark lavender bags, 1 dull red bag, 4 vibrant turquoise bags, 5 faded fuchsia bags.\n" +
"dark teal bags contain 2 dotted brown bags, 5 clear salmon bags, 5 wavy coral bags, 3 light fuchsia bags.\n" +
"shiny cyan bags contain 1 mirrored gold bag, 1 mirrored aqua bag, 1 posh magenta bag.\n" +
"pale red bags contain 1 vibrant turquoise bag.\n" +
"drab coral bags contain 2 faded tomato bags, 3 plaid aqua bags.\n" +
"clear white bags contain 1 dull white bag, 5 shiny tan bags, 1 mirrored yellow bag, 1 wavy chartreuse bag.\n" +
"clear crimson bags contain 5 muted white bags, 1 vibrant yellow bag.\n" +
"posh lavender bags contain 2 mirrored aqua bags, 4 faded aqua bags, 2 light red bags.\n" +
"dim violet bags contain 4 dull orange bags, 5 dull turquoise bags, 5 dark purple bags.\n" +
"pale beige bags contain 3 wavy indigo bags, 5 dark plum bags, 2 dark purple bags, 2 dim turquoise bags.\n" +
"striped brown bags contain 4 bright lime bags, 5 wavy coral bags.\n" +
"shiny blue bags contain 2 wavy coral bags, 2 dotted plum bags.\n" +
"pale black bags contain 5 clear maroon bags, 4 striped crimson bags, 4 dark gold bags.\n" +
"drab plum bags contain 3 clear gold bags.\n" +
"posh beige bags contain 3 faded silver bags, 3 dim gold bags, 5 shiny gold bags.\n" +
"clear lavender bags contain 1 clear white bag.\n" +
"light lavender bags contain 4 dotted violet bags, 3 dull violet bags, 4 dark maroon bags, 2 striped violet bags.\n" +
"plaid brown bags contain 4 dotted yellow bags.\n" +
"dotted green bags contain 3 dull red bags, 2 faded green bags, 3 muted white bags, 1 dim turquoise bag.\n" +
"muted olive bags contain 4 vibrant tan bags, 3 dotted black bags.\n" +
"light turquoise bags contain 3 wavy blue bags, 4 dim plum bags, 4 dim chartreuse bags.\n" +
"dim salmon bags contain 2 mirrored yellow bags.\n" +
"shiny white bags contain 1 mirrored violet bag.\n" +
"drab olive bags contain 3 striped crimson bags, 3 dotted maroon bags.\n" +
"drab silver bags contain 5 shiny cyan bags, 1 dull teal bag, 4 dark lime bags.\n" +
"vibrant turquoise bags contain 4 striped crimson bags, 5 dark plum bags, 3 dark lavender bags, 5 dark purple bags.\n" +
"shiny purple bags contain 1 posh beige bag, 1 dark indigo bag, 4 plaid fuchsia bags, 5 dotted green bags.\n" +
"bright brown bags contain 1 striped lavender bag, 5 light gold bags, 1 dim maroon bag.\n" +
"dotted violet bags contain 3 striped blue bags.\n" +
"bright crimson bags contain 2 muted gold bags, 3 vibrant turquoise bags, 3 wavy green bags, 5 dull white bags.\n" +
"striped tan bags contain 2 wavy gray bags, 4 bright gray bags, 1 plaid cyan bag, 1 vibrant magenta bag.\n" +
"dull green bags contain 1 bright brown bag, 5 posh beige bags, 5 mirrored crimson bags, 1 dark silver bag.\n" +
"mirrored lime bags contain 4 muted tomato bags, 5 wavy chartreuse bags, 5 dotted plum bags.\n" +
"striped beige bags contain 2 striped bronze bags, 2 shiny yellow bags, 4 pale gray bags, 2 vibrant tomato bags.\n" +
"vibrant lime bags contain 4 striped crimson bags, 2 pale silver bags, 4 clear purple bags.\n" +
"pale blue bags contain 1 bright cyan bag, 1 shiny fuchsia bag, 5 wavy turquoise bags, 5 dotted silver bags.\n" +
"striped maroon bags contain 1 muted lime bag.\n" +
"faded chartreuse bags contain 2 mirrored white bags.\n" +
"mirrored crimson bags contain 2 dotted gold bags, 5 shiny red bags, 5 wavy red bags.\n" +
"dull beige bags contain 2 posh red bags, 1 vibrant turquoise bag, 5 dark plum bags, 5 clear aqua bags.\n" +
"wavy green bags contain 3 shiny crimson bags.\n" +
"striped salmon bags contain 2 dim plum bags, 2 mirrored indigo bags.\n" +
"faded coral bags contain 3 muted tomato bags, 1 light chartreuse bag.\n" +
"dark blue bags contain 2 shiny violet bags.\n" +
"shiny gold bags contain 1 dull lime bag, 2 pale coral bags, 1 wavy silver bag, 5 muted black bags.\n" +
"clear turquoise bags contain 1 posh magenta bag.\n" +
"dotted salmon bags contain 2 mirrored lime bags, 1 mirrored salmon bag.\n" +
"pale gray bags contain 3 bright blue bags, 5 muted tan bags.\n" +
"shiny teal bags contain 5 pale indigo bags.\n" +
"drab beige bags contain 5 faded indigo bags, 5 vibrant bronze bags, 4 pale lime bags, 3 dark black bags.\n" +
"vibrant white bags contain 5 striped violet bags, 3 bright blue bags, 2 dim gold bags.\n" +
"posh aqua bags contain 5 plaid violet bags, 3 drab chartreuse bags.\n" +
"plaid gray bags contain 5 dotted cyan bags, 2 mirrored silver bags.\n" +
"clear indigo bags contain 1 bright tan bag, 2 dull yellow bags, 5 mirrored lavender bags, 2 mirrored fuchsia bags.\n" +
"mirrored turquoise bags contain 3 faded chartreuse bags, 2 drab turquoise bags.\n" +
"dark chartreuse bags contain 2 mirrored silver bags, 3 vibrant crimson bags, 4 shiny green bags, 1 pale brown bag.\n" +
"pale tomato bags contain 2 light gold bags.\n" +
"mirrored purple bags contain 1 light magenta bag, 3 shiny yellow bags, 4 striped lavender bags.\n" +
"mirrored blue bags contain 3 posh magenta bags, 4 dark lavender bags, 5 striped crimson bags.\n" +
"dull purple bags contain 4 mirrored indigo bags, 2 wavy bronze bags, 4 muted white bags, 4 shiny crimson bags.\n" +
"mirrored teal bags contain 5 dim teal bags, 4 dark lime bags, 4 mirrored yellow bags, 3 mirrored turquoise bags.\n" +
"drab gold bags contain 3 dark maroon bags, 2 plaid beige bags.\n" +
"dotted gold bags contain 2 striped crimson bags, 3 clear red bags.\n" +
"posh fuchsia bags contain 4 dim cyan bags, 4 vibrant turquoise bags, 4 dotted plum bags.\n" +
"dim lime bags contain 4 posh indigo bags.\n" +
"dim tan bags contain 3 pale indigo bags, 1 striped turquoise bag, 4 plaid coral bags.\n" +
"plaid red bags contain 5 wavy gold bags, 1 muted plum bag, 2 vibrant plum bags, 1 dim brown bag.\n" +
"light orange bags contain 4 dotted teal bags, 4 dotted yellow bags.\n" +
"dark green bags contain 1 dull cyan bag.\n" +
"posh coral bags contain 3 shiny green bags, 3 dotted red bags, 1 drab silver bag.\n" +
"striped orange bags contain 3 plaid plum bags.\n" +
"pale purple bags contain 1 vibrant coral bag, 2 shiny olive bags, 1 clear orange bag.\n" +
"bright fuchsia bags contain 3 bright yellow bags.\n" +
"striped red bags contain 2 bright orange bags, 3 mirrored yellow bags, 1 dotted coral bag, 3 clear aqua bags.\n" +
"faded plum bags contain 2 shiny tan bags, 4 dull tan bags.\n" +
"striped gold bags contain 1 shiny crimson bag.\n" +
"faded olive bags contain 2 shiny lavender bags, 5 posh fuchsia bags, 3 pale gray bags.\n" +
"pale aqua bags contain 3 posh beige bags.\n" +
"dotted plum bags contain 4 clear red bags.\n" +
"dotted chartreuse bags contain 1 light white bag, 5 drab turquoise bags.\n" +
"striped tomato bags contain 4 dim plum bags, 1 striped black bag, 4 drab black bags.\n" +
"shiny lime bags contain 3 drab coral bags, 1 drab green bag, 4 muted salmon bags.\n" +
"dim orange bags contain 2 striped chartreuse bags.\n" +
"plaid tan bags contain 5 light chartreuse bags, 1 dotted maroon bag, 2 clear silver bags.\n" +
"dull olive bags contain 3 muted gold bags, 5 dotted gold bags, 4 muted white bags, 1 shiny violet bag.\n" +
"dotted beige bags contain 1 clear tan bag.\n" +
"dotted silver bags contain 2 faded crimson bags.\n" +
"muted black bags contain 3 clear red bags, 3 dotted maroon bags.\n" +
"clear purple bags contain 5 dotted plum bags.\n" +
"striped indigo bags contain 5 mirrored blue bags, 2 striped violet bags, 3 striped yellow bags.\n" +
"faded teal bags contain 2 light black bags, 1 vibrant maroon bag.\n" +
"drab red bags contain 3 dark tan bags, 4 pale red bags, 5 faded salmon bags, 3 dotted tomato bags.\n" +
"muted aqua bags contain 2 striped purple bags, 3 vibrant orange bags, 3 bright cyan bags.\n" +
"mirrored black bags contain 4 muted yellow bags, 5 light fuchsia bags.\n" +
"dark gold bags contain 2 dark indigo bags, 3 muted magenta bags, 3 dull white bags.\n" +
"dotted lime bags contain 4 shiny blue bags, 3 striped fuchsia bags.\n" +
"posh brown bags contain 2 muted beige bags.\n" +
"muted green bags contain 5 bright white bags, 5 dim gold bags.\n" +
"vibrant salmon bags contain 1 pale green bag, 5 light maroon bags, 4 striped violet bags.\n" +
"drab white bags contain 2 drab fuchsia bags, 4 dull beige bags, 2 pale coral bags, 1 shiny tan bag.\n" +
"light lime bags contain 4 dotted salmon bags, 5 dark fuchsia bags, 2 vibrant indigo bags.\n" +
"shiny tomato bags contain 5 wavy green bags, 2 striped blue bags, 3 faded green bags, 3 muted tomato bags.\n" +
"mirrored aqua bags contain 2 dark gold bags, 2 shiny green bags.\n" +
"plaid cyan bags contain 5 shiny brown bags, 2 pale aqua bags, 3 striped turquoise bags, 5 plaid yellow bags.\n" +
"light crimson bags contain 3 bright silver bags, 2 dark lime bags.\n" +
"dull cyan bags contain 3 pale gray bags, 5 light beige bags, 3 striped blue bags.\n" +
"muted plum bags contain 2 muted turquoise bags, 5 wavy lavender bags, 3 striped blue bags, 3 striped gray bags.\n" +
"posh tan bags contain 4 dark brown bags, 1 drab crimson bag.\n" +
"clear yellow bags contain 3 striped purple bags, 4 faded blue bags.\n" +
"wavy fuchsia bags contain 2 light beige bags, 3 faded fuchsia bags.\n" +
"pale gold bags contain 4 faded green bags, 5 clear bronze bags, 5 vibrant aqua bags, 3 muted cyan bags.\n" +
"shiny gray bags contain 3 plaid chartreuse bags.\n" +
"bright indigo bags contain 3 drab fuchsia bags.\n" +
"drab black bags contain 3 shiny brown bags, 3 posh blue bags.\n" +
"dark crimson bags contain 5 faded olive bags, 1 mirrored green bag.\n" +
"vibrant silver bags contain 1 dark black bag, 2 drab tan bags.\n" +
"shiny green bags contain 4 dark plum bags, 4 drab fuchsia bags, 3 dim plum bags, 3 mirrored gold bags.\n" +
"plaid crimson bags contain 1 dim olive bag, 5 bright brown bags, 5 dull lavender bags.\n" +
"dark salmon bags contain 2 light gold bags, 4 shiny blue bags.\n" +
"faded blue bags contain 4 striped crimson bags, 1 muted tomato bag.\n" +
"wavy magenta bags contain 5 wavy purple bags, 3 bright yellow bags, 3 shiny maroon bags, 1 wavy orange bag.\n" +
"striped cyan bags contain 1 dotted plum bag, 1 bright coral bag.\n" +
"faded crimson bags contain 5 pale aqua bags, 3 light blue bags.\n" +
"muted gray bags contain 3 shiny tomato bags.\n" +
"striped aqua bags contain 3 dim crimson bags.\n" +
"wavy salmon bags contain 5 drab bronze bags, 4 light maroon bags.\n" +
"drab fuchsia bags contain 3 faded black bags, 4 shiny crimson bags, 5 shiny tan bags.\n" +
"clear black bags contain 4 dark crimson bags, 5 pale lavender bags.\n" +
"dotted orange bags contain 1 dotted coral bag, 1 shiny gold bag, 3 wavy coral bags.\n" +
"plaid tomato bags contain 4 wavy orange bags.\n" +
"dark white bags contain 3 posh cyan bags.\n" +
"wavy lime bags contain 4 faded silver bags, 3 vibrant gold bags, 3 vibrant coral bags.\n" +
"faded tomato bags contain 1 striped bronze bag, 4 dotted green bags.\n" +
"mirrored salmon bags contain 1 faded crimson bag, 3 muted gold bags, 4 clear aqua bags.\n" +
"drab maroon bags contain 5 clear silver bags, 4 vibrant cyan bags, 5 faded gold bags.\n" +
"dull violet bags contain 4 shiny tomato bags.\n" +
"bright orange bags contain 4 bright tan bags.\n" +
"plaid magenta bags contain 3 light fuchsia bags.\n" +
"shiny turquoise bags contain 5 muted bronze bags, 4 bright lavender bags, 4 dark tan bags.\n" +
"pale fuchsia bags contain 4 dull crimson bags, 3 wavy red bags, 5 dark gold bags.\n" +
"plaid fuchsia bags contain 2 dim cyan bags, 5 bright coral bags.\n" +
"dark indigo bags contain 3 dotted teal bags, 3 striped blue bags.\n" +
"vibrant bronze bags contain 4 mirrored plum bags, 5 faded silver bags.\n" +
"light tomato bags contain 3 posh fuchsia bags, 4 mirrored magenta bags, 1 muted gold bag, 3 dim green bags.\n" +
"shiny silver bags contain 2 dim yellow bags, 1 wavy coral bag.\n" +
"bright bronze bags contain 3 plaid coral bags, 2 wavy coral bags, 2 bright gray bags.\n" +
"plaid turquoise bags contain 3 dim lime bags.\n" +
"plaid teal bags contain 1 vibrant teal bag, 3 dark indigo bags, 4 plaid fuchsia bags.\n" +
"striped purple bags contain 2 striped chartreuse bags, 2 clear chartreuse bags, 4 dark cyan bags, 5 striped salmon bags.\n" +
"pale orange bags contain 5 muted green bags.\n" +
"light green bags contain 2 plaid plum bags.\n" +
"plaid purple bags contain 4 muted lime bags.\n" +
"dark cyan bags contain no other bags.\n" +
"wavy teal bags contain 5 dull black bags, 3 striped teal bags, 4 dotted cyan bags, 5 vibrant olive bags.\n" +
"shiny indigo bags contain 3 dull lavender bags.\n" +
"bright beige bags contain 1 bright bronze bag.\n" +
"light white bags contain no other bags.\n" +
"muted red bags contain 3 light tan bags, 4 clear cyan bags.\n" +
"light coral bags contain 2 dull aqua bags, 1 bright beige bag, 4 wavy maroon bags, 3 posh tan bags.\n" +
"bright violet bags contain 2 drab beige bags, 3 shiny fuchsia bags, 2 shiny cyan bags, 5 light gray bags.\n" +
"striped fuchsia bags contain 1 striped black bag, 4 shiny white bags, 3 dim plum bags.\n" +
"vibrant teal bags contain 1 faded tomato bag, 3 dim gray bags.\n" +
"striped turquoise bags contain 3 wavy silver bags, 3 bright orange bags, 5 plaid coral bags, 1 clear red bag.\n" +
"mirrored fuchsia bags contain 1 dotted green bag, 1 plaid cyan bag.\n" +
"dim green bags contain 1 posh olive bag, 1 dim silver bag.\n" +
"faded orange bags contain 4 drab beige bags, 1 vibrant salmon bag, 2 plaid orange bags.\n" +
"bright tomato bags contain 4 dim aqua bags, 2 posh beige bags, 4 striped purple bags.\n" +
"faded lime bags contain 1 bright tan bag.\n" +
"mirrored indigo bags contain 4 shiny tan bags, 3 faded blue bags.\n" +
"vibrant orange bags contain 3 vibrant olive bags, 2 bright olive bags, 3 wavy violet bags.\n" +
"plaid beige bags contain 2 shiny violet bags, 3 faded violet bags.\n" +
"muted purple bags contain 3 light beige bags, 3 bright tan bags.\n" +
"mirrored orange bags contain 5 plaid lime bags, 4 dotted olive bags.\n" +
"wavy purple bags contain 3 dim chartreuse bags.\n" +
"wavy blue bags contain 5 striped blue bags, 5 posh olive bags.\n" +
"dull black bags contain 3 dotted plum bags.\n" +
"drab bronze bags contain 5 dim olive bags, 3 vibrant gray bags, 3 pale green bags, 1 dull cyan bag.\n" +
"pale coral bags contain 5 dim gold bags, 1 vibrant bronze bag.\n" +
"dull orange bags contain 1 shiny turquoise bag.\n" +
"mirrored gray bags contain 1 mirrored plum bag, 5 dim silver bags.\n" +
"drab cyan bags contain 1 dark fuchsia bag, 1 striped salmon bag, 4 plaid beige bags, 5 dim olive bags.\n" +
"dark bronze bags contain 2 pale salmon bags, 5 dull magenta bags, 5 clear indigo bags, 3 wavy plum bags.\n" +
"dim tomato bags contain 5 striped lavender bags.\n" +
"faded white bags contain 3 striped lime bags, 4 dotted orange bags.\n" +
"dull teal bags contain 4 vibrant yellow bags.\n" +
"clear teal bags contain 4 wavy gold bags.\n" +
"muted tan bags contain 3 faded green bags.\n" +
"dim gold bags contain no other bags.\n" +
"dim turquoise bags contain 3 mirrored plum bags, 3 bright orange bags, 1 muted tan bag.\n" +
"striped silver bags contain 4 dull lime bags, 5 striped blue bags.\n" +
"striped yellow bags contain 2 vibrant crimson bags.\n" +
"striped violet bags contain 1 clear beige bag, 2 dim indigo bags, 3 muted lime bags.\n" +
"posh cyan bags contain 4 faded blue bags, 2 vibrant magenta bags, 3 dull lime bags, 3 shiny blue bags.\n" +
"vibrant brown bags contain 5 shiny violet bags.\n" +
"pale chartreuse bags contain 1 wavy teal bag, 1 muted maroon bag, 5 clear turquoise bags.\n" +
"vibrant indigo bags contain 5 drab teal bags, 1 bright aqua bag.\n" +
"bright green bags contain 4 drab silver bags, 5 shiny gold bags.\n" +
"dim black bags contain 1 faded violet bag.\n" +
"muted orange bags contain 3 dull yellow bags, 1 vibrant blue bag, 3 plaid tan bags, 2 dotted teal bags.\n" +
"muted crimson bags contain 4 muted silver bags, 5 shiny lavender bags, 3 posh beige bags, 2 shiny purple bags.\n" +
"clear tomato bags contain 4 dotted teal bags, 2 vibrant olive bags, 5 plaid magenta bags, 4 bright chartreuse bags.\n" +
"vibrant fuchsia bags contain 5 dim cyan bags, 4 light white bags, 1 dull orange bag, 5 dark gold bags.\n" +
"muted magenta bags contain no other bags.\n" +
"dim brown bags contain 1 striped red bag, 4 posh fuchsia bags, 3 mirrored gold bags.\n" +
"dim plum bags contain 1 vibrant turquoise bag, 1 clear chartreuse bag, 3 faded brown bags.\n" +
"plaid gold bags contain 5 drab tomato bags.\n" +
"wavy tomato bags contain 3 dark gold bags, 1 light orange bag, 4 muted salmon bags, 2 muted olive bags.\n" +
"pale olive bags contain 2 plaid cyan bags.\n" +
"wavy plum bags contain 4 faded aqua bags, 5 dull olive bags, 3 bright lavender bags, 2 drab chartreuse bags.\n" +
"posh tomato bags contain 5 drab blue bags.\n" +
"dull gray bags contain 5 dull olive bags, 3 mirrored coral bags, 4 striped lavender bags.\n" +
"posh plum bags contain 5 dull cyan bags, 2 dotted orange bags, 3 plaid indigo bags, 5 light beige bags.\n" +
"striped plum bags contain 2 drab white bags, 2 mirrored yellow bags, 3 dim crimson bags, 2 pale salmon bags.\n" +
"light beige bags contain 3 dark cyan bags, 3 striped crimson bags.\n" +
"dark maroon bags contain 5 light tan bags, 5 faded green bags, 3 striped gold bags, 4 bright aqua bags.\n" +
"faded brown bags contain 5 dotted gold bags, 3 striped turquoise bags, 4 bright tan bags, 3 clear chartreuse bags.\n" +
"striped lime bags contain 4 dim gold bags, 5 pale coral bags, 4 mirrored plum bags.\n" +
"light olive bags contain 1 shiny white bag, 3 dotted plum bags, 3 clear orange bags, 3 posh turquoise bags.\n" +
"light magenta bags contain 2 shiny turquoise bags.\n" +
"dotted turquoise bags contain 5 posh olive bags.\n" +
"dark violet bags contain 1 faded blue bag, 1 shiny green bag.\n" +
"shiny plum bags contain 2 dotted blue bags, 1 clear gold bag.\n" +
"striped bronze bags contain 5 dim chartreuse bags, 1 shiny violet bag.\n" +
"vibrant lavender bags contain 1 faded fuchsia bag, 2 shiny blue bags, 2 dotted plum bags.\n" +
"dotted crimson bags contain 2 dark cyan bags, 1 dim chartreuse bag, 4 light red bags.\n" +
"posh blue bags contain 2 bright silver bags, 4 drab beige bags, 2 dim salmon bags.\n" +
"plaid orange bags contain 3 dark lime bags.\n" +
"wavy gold bags contain 1 drab olive bag, 1 shiny cyan bag, 3 drab brown bags.\n" +
"muted lime bags contain 2 dull teal bags.\n" +
"vibrant magenta bags contain 3 muted gray bags.\n" +
"posh silver bags contain 4 bright lavender bags, 5 light gold bags, 2 posh beige bags, 2 dull crimson bags.\n" +
"shiny bronze bags contain 5 pale lavender bags, 3 plaid bronze bags, 3 mirrored blue bags, 4 bright olive bags.\n" +
"dotted tomato bags contain 2 drab yellow bags.\n" +
"posh violet bags contain 4 muted fuchsia bags, 1 dark blue bag, 1 shiny indigo bag.\n" +
"wavy turquoise bags contain 5 shiny yellow bags.\n" +
"plaid lavender bags contain 3 dim lavender bags, 3 dotted salmon bags.\n" +
"striped teal bags contain 5 mirrored crimson bags, 1 mirrored violet bag, 1 dark violet bag, 5 dotted yellow bags.\n" +
"faded black bags contain 5 posh red bags.\n" +
"faded tan bags contain 1 mirrored violet bag, 4 dark lime bags, 4 dim green bags, 4 muted green bags.\n" +
"clear brown bags contain 3 pale coral bags, 1 plaid coral bag, 4 pale gray bags.\n" +
"light violet bags contain 5 dim plum bags, 4 muted violet bags, 3 drab teal bags.\n" +
"dotted gray bags contain 4 mirrored white bags, 3 vibrant lavender bags, 4 dark lime bags, 5 vibrant coral bags.\n" +
"faded yellow bags contain 2 plaid bronze bags, 1 dark purple bag, 5 shiny crimson bags, 3 shiny tan bags.\n" +
"shiny violet bags contain 4 dull lime bags, 4 muted white bags, 2 dark cyan bags.\n" +
"faded gold bags contain 5 plaid aqua bags.\n" +
"bright salmon bags contain 2 shiny lavender bags, 5 plaid red bags.\n" +
"clear violet bags contain 4 posh turquoise bags.\n" +
"light silver bags contain 1 light salmon bag, 2 clear aqua bags, 4 mirrored plum bags, 2 striped silver bags.\n" +
"pale cyan bags contain 4 vibrant orange bags, 3 light salmon bags, 4 striped cyan bags, 4 wavy gray bags.\n" +
"wavy cyan bags contain 3 light orange bags, 1 bright yellow bag, 2 pale fuchsia bags.\n" +
"clear maroon bags contain 1 dotted coral bag, 4 plaid yellow bags.\n" +
"striped blue bags contain 5 light white bags, 2 dull lime bags, 1 shiny maroon bag.\n" +
"clear red bags contain no other bags.\n" +
"pale tan bags contain 5 light gold bags, 5 dark brown bags, 1 wavy black bag, 5 drab blue bags.\n" +
"mirrored magenta bags contain 4 dotted plum bags, 3 posh beige bags.\n" +
"dull maroon bags contain 1 mirrored fuchsia bag.\n" +
"dim silver bags contain 5 striped crimson bags, 1 light beige bag.\n" +
"plaid olive bags contain 3 shiny white bags, 2 bright lavender bags.\n" +
"shiny magenta bags contain 3 dotted gray bags, 1 shiny blue bag, 3 faded silver bags, 2 dark tomato bags.\n" +
"mirrored green bags contain 4 dim cyan bags, 2 bright tan bags.\n" +
"faded silver bags contain 1 dark plum bag, 5 dotted plum bags, 1 dark cyan bag, 5 dull red bags.\n" +
"vibrant cyan bags contain 5 vibrant lavender bags.\n" +
"shiny brown bags contain 4 striped blue bags, 1 light beige bag, 1 muted tan bag, 5 bright orange bags.\n" +
"wavy maroon bags contain 3 vibrant white bags, 1 pale red bag, 5 striped yellow bags, 4 plaid bronze bags.\n" +
"shiny aqua bags contain 5 dotted salmon bags, 5 bright maroon bags, 1 wavy red bag.\n" +
"clear cyan bags contain 2 striped red bags, 5 plaid coral bags, 2 dark fuchsia bags.\n" +
"dotted red bags contain 5 dim orange bags, 4 striped tan bags.\n" +
"vibrant green bags contain 1 dark plum bag.\n" +
"striped black bags contain 5 dull white bags.\n" +
"dotted indigo bags contain 2 vibrant orange bags, 5 vibrant lavender bags, 4 shiny bronze bags.\n" +
"dark tan bags contain 1 muted tan bag.\n" +
"dotted yellow bags contain 3 dark cyan bags, 2 shiny maroon bags.\n" +
"dull chartreuse bags contain 4 vibrant red bags, 4 dim salmon bags, 5 drab salmon bags.\n" +
"shiny beige bags contain 3 dotted yellow bags, 2 muted tomato bags, 4 wavy red bags.\n" +
"faded indigo bags contain 4 faded green bags.\n" +
"light chartreuse bags contain 1 mirrored violet bag, 5 striped lavender bags.\n" +
"dark red bags contain 5 dark green bags, 3 clear beige bags, 5 wavy tan bags, 4 striped cyan bags.\n" +
"muted blue bags contain 3 dark beige bags, 2 bright lavender bags, 5 dull teal bags.\n" +
"mirrored olive bags contain 5 dark lavender bags, 1 clear blue bag.\n" +
"pale yellow bags contain 2 dark chartreuse bags, 4 posh silver bags, 2 dim crimson bags, 3 wavy gold bags.\n" +
"dull brown bags contain 1 light aqua bag, 2 pale aqua bags, 1 faded red bag, 1 wavy white bag.\n" +
"clear chartreuse bags contain 2 wavy coral bags.\n" +
"bright white bags contain 4 light fuchsia bags, 5 drab turquoise bags, 5 striped lime bags.\n" +
"light gray bags contain 4 mirrored violet bags, 5 dotted maroon bags, 3 pale lime bags.\n" +
"mirrored cyan bags contain 4 mirrored salmon bags, 2 drab olive bags.\n" +
"dark orange bags contain 3 vibrant lavender bags.\n" +
"drab chartreuse bags contain 2 wavy orange bags.\n" +
"muted white bags contain no other bags.\n" +
"dull red bags contain 1 faded fuchsia bag, 2 dark purple bags, 4 clear red bags.\n" +
"shiny red bags contain 2 bright gray bags, 4 drab blue bags, 2 bright aqua bags.\n" +
"vibrant gold bags contain 3 shiny tan bags, 5 mirrored gold bags.\n" +
"wavy red bags contain 1 dark plum bag, 3 striped blue bags, 4 light white bags, 1 wavy silver bag.\n" +
"mirrored coral bags contain 4 dark indigo bags.\n" +
"mirrored beige bags contain 1 muted white bag.\n" +
"faded bronze bags contain 5 bright aqua bags, 2 dotted chartreuse bags, 2 faded tomato bags.\n" +
"bright plum bags contain 3 drab chartreuse bags.\n" +
"shiny coral bags contain 2 dark yellow bags, 5 dark blue bags, 4 dotted green bags.\n" +
"mirrored yellow bags contain 1 dotted plum bag.\n" +
"faded fuchsia bags contain no other bags.\n" +
"bright magenta bags contain 4 muted orange bags, 3 striped red bags, 5 light bronze bags.\n" +
"drab aqua bags contain 5 dotted indigo bags, 3 dim gray bags.\n" +
"faded violet bags contain 2 shiny gold bags.\n" +
"dull lavender bags contain 1 drab chartreuse bag, 2 shiny plum bags, 4 vibrant tan bags, 5 dark tomato bags.\n" +
"shiny lavender bags contain 2 light fuchsia bags.\n" +
"dotted brown bags contain 5 shiny brown bags, 4 light fuchsia bags, 3 plaid cyan bags.\n" +
"dark olive bags contain 2 wavy red bags, 5 striped lavender bags, 4 vibrant lavender bags, 2 shiny gold bags.\n" +
"dotted black bags contain 1 dark lime bag.\n" +
"faded aqua bags contain 2 dim turquoise bags, 3 muted green bags, 5 clear aqua bags.\n" +
"plaid yellow bags contain 1 striped turquoise bag, 4 striped lime bags.\n" +
"pale bronze bags contain 4 muted tan bags, 5 dotted fuchsia bags.\n" +
"muted coral bags contain 2 striped bronze bags, 1 dark violet bag.\n" +
"dim indigo bags contain 1 shiny red bag.\n" +
"drab gray bags contain 3 posh white bags, 2 plaid indigo bags.\n" +
"pale silver bags contain 1 faded coral bag, 5 shiny lavender bags, 5 wavy lime bags.\n" +
"wavy silver bags contain 1 vibrant bronze bag.\n" +
"wavy crimson bags contain 1 mirrored orange bag.\n" +
"dotted aqua bags contain 4 drab plum bags, 5 faded indigo bags, 3 dark violet bags, 3 pale gray bags.\n" +
"mirrored violet bags contain 5 mirrored white bags.\n" +
"dotted white bags contain 3 muted plum bags, 3 bright bronze bags, 2 dark salmon bags.\n" +
"bright coral bags contain 5 mirrored gray bags.\n" +
"dim aqua bags contain 3 mirrored plum bags, 3 plaid olive bags, 1 light gold bag.\n" +
"drab crimson bags contain 5 faded purple bags.\n" +
"pale crimson bags contain 4 dotted orange bags, 2 faded silver bags.\n" +
"mirrored tan bags contain 3 posh gray bags, 2 dark silver bags, 3 dim tan bags.\n" +
"clear olive bags contain 1 dim yellow bag, 1 bright blue bag, 3 bright tomato bags.\n" +
"dim blue bags contain 3 drab black bags, 1 light blue bag, 5 light orange bags, 5 bright white bags.\n" +
"mirrored chartreuse bags contain 4 drab beige bags, 1 pale indigo bag.\n" +
"dark turquoise bags contain 1 vibrant black bag, 1 light turquoise bag, 5 pale aqua bags.\n" +
"posh yellow bags contain 4 dim teal bags, 2 dark black bags.\n" +
"clear aqua bags contain 2 drab teal bags, 5 light gold bags, 4 dim crimson bags.\n" +
"dotted purple bags contain 4 striped salmon bags.\n" +
"light indigo bags contain 3 faded red bags, 2 shiny crimson bags, 3 wavy green bags, 2 striped coral bags.\n" +
"vibrant yellow bags contain 1 dotted cyan bag, 3 posh silver bags.\n" +
"faded lavender bags contain 5 dull cyan bags.\n" +
"dull magenta bags contain 2 dotted green bags.\n" +
"posh indigo bags contain 1 dull lime bag, 4 striped lavender bags, 1 faded silver bag.\n" +
"light aqua bags contain 3 posh maroon bags, 3 dotted black bags, 4 muted tomato bags, 4 mirrored magenta bags.\n" +
"mirrored maroon bags contain 3 light aqua bags.\n" +
"dim red bags contain 2 muted green bags, 1 vibrant tan bag.\n" +
"wavy yellow bags contain 3 dotted cyan bags.\n" +
"bright aqua bags contain 1 dotted maroon bag, 1 muted white bag, 3 striped turquoise bags.\n" +
"clear fuchsia bags contain 2 mirrored fuchsia bags, 4 bright white bags, 3 mirrored white bags, 1 bright coral bag.\n" +
"dotted olive bags contain 3 pale aqua bags, 4 light gold bags, 5 dim yellow bags.\n" +
"light black bags contain 4 clear tan bags, 5 wavy lime bags, 3 dim fuchsia bags, 3 pale indigo bags.\n" +
"dark purple bags contain 5 dark cyan bags.\n" +
"dim purple bags contain 2 wavy crimson bags, 2 dim chartreuse bags, 3 shiny yellow bags, 1 light black bag.\n" +
"plaid salmon bags contain 1 faded lime bag.\n" +
"dotted cyan bags contain 2 shiny tomato bags, 1 mirrored plum bag.\n" +
"posh maroon bags contain 1 light bronze bag, 4 muted tan bags, 5 vibrant plum bags.\n" +
"dim fuchsia bags contain 3 dim plum bags, 5 dark gray bags.\n" +
"bright tan bags contain 1 shiny maroon bag, 1 light beige bag, 2 faded fuchsia bags.\n" +
"plaid green bags contain 3 wavy cyan bags, 5 dark chartreuse bags, 5 dark blue bags.\n" +
"plaid black bags contain 4 dull olive bags, 4 mirrored white bags, 4 plaid white bags.\n" +
"clear beige bags contain 1 dull tan bag.\n" +
"mirrored tomato bags contain 5 drab tomato bags, 1 plaid bronze bag.\n" +
"dim yellow bags contain 4 dark indigo bags, 4 bright silver bags, 3 muted purple bags.\n" +
"clear tan bags contain 4 light gold bags, 3 bright lavender bags, 2 mirrored fuchsia bags.\n" +
"dim lavender bags contain 3 vibrant aqua bags.\n" +
"striped white bags contain 3 posh tomato bags, 3 dark gold bags, 2 clear olive bags, 1 dotted bronze bag.\n" +
"wavy bronze bags contain 1 plaid lime bag, 4 plaid yellow bags, 1 bright white bag.\n" +
"shiny orange bags contain 3 drab chartreuse bags, 1 shiny gold bag.\n" +
"dim coral bags contain 2 faded silver bags.\n" +
"bright gold bags contain 5 dim aqua bags, 2 drab bronze bags.\n" +
"vibrant black bags contain 4 dotted green bags, 2 light brown bags.\n" +
"bright teal bags contain 1 light gold bag, 2 bright aqua bags.\n" +
"drab brown bags contain 2 wavy blue bags, 5 wavy orange bags.\n" +
"dull yellow bags contain 4 striped green bags, 4 bright lime bags, 2 faded tan bags, 5 dotted green bags.\n" +
"vibrant maroon bags contain 4 mirrored fuchsia bags.\n" +
"vibrant gray bags contain 3 faded turquoise bags, 5 pale black bags, 3 drab aqua bags.\n" +
"pale white bags contain 3 shiny beige bags.\n" +
"dim beige bags contain 1 pale fuchsia bag, 1 muted lime bag, 3 dotted plum bags.\n" +
"drab blue bags contain 4 dim crimson bags, 4 shiny beige bags, 2 bright silver bags.\n" +
"dark black bags contain 5 light beige bags, 5 muted gold bags.\n" +
"plaid bronze bags contain 3 wavy indigo bags.\n" +
"posh salmon bags contain 3 dark salmon bags, 2 posh purple bags, 5 dim yellow bags.\n" +
"mirrored lavender bags contain 4 wavy chartreuse bags, 4 plaid bronze bags.\n" +
"muted brown bags contain 1 drab magenta bag, 2 vibrant plum bags, 4 clear purple bags, 4 dark blue bags.\n" +
"wavy white bags contain 2 striped red bags.\n" +
"dark gray bags contain 5 bright blue bags, 2 light brown bags.\n" +
"mirrored silver bags contain 2 dotted chartreuse bags, 1 dotted fuchsia bag, 4 mirrored lavender bags, 2 mirrored white bags.\n" +
"wavy brown bags contain 5 posh teal bags, 4 drab white bags, 5 shiny chartreuse bags, 4 light maroon bags.\n" +
"posh gray bags contain 1 striped turquoise bag, 4 muted crimson bags, 3 clear yellow bags, 3 striped crimson bags.\n" +
"dull tomato bags contain 5 drab purple bags, 4 faded yellow bags.\n" +
"dotted blue bags contain 2 dull cyan bags, 3 dim plum bags.\n" +
"vibrant tomato bags contain 5 plaid coral bags, 5 pale fuchsia bags.\n" +
"wavy beige bags contain 3 dull magenta bags, 3 dotted beige bags, 3 bright white bags, 2 striped fuchsia bags.\n" +
"plaid lime bags contain 3 light blue bags, 3 drab brown bags, 1 drab teal bag.\n" +
"dim white bags contain 5 wavy coral bags, 5 vibrant bronze bags, 2 dark cyan bags, 2 dark plum bags.\n" +
"bright yellow bags contain 4 pale aqua bags, 2 dim turquoise bags, 4 faded brown bags.\n" +
"plaid maroon bags contain 1 dark teal bag, 4 vibrant white bags, 5 striped coral bags, 4 shiny white bags.\n" +
"pale lavender bags contain 2 shiny violet bags.\n" +
"drab magenta bags contain 5 bright gray bags.\n" +
"clear salmon bags contain 4 wavy purple bags, 1 bright olive bag, 1 clear yellow bag.\n" +
"dark yellow bags contain 1 bright bronze bag, 3 striped plum bags.\n" +
"posh bronze bags contain 1 shiny maroon bag, 4 drab olive bags, 2 mirrored green bags, 5 light silver bags.\n" +
"drab green bags contain 3 dim orange bags, 5 vibrant chartreuse bags.\n" +
"light fuchsia bags contain 2 wavy silver bags, 4 muted white bags, 5 muted black bags, 1 faded green bag.\n" +
"dark plum bags contain 4 bright tan bags, 4 dull lime bags, 2 faded fuchsia bags, 4 shiny maroon bags.\n" +
"light teal bags contain 4 light lavender bags.\n" +
"vibrant tan bags contain 1 vibrant bronze bag.\n" +
"muted teal bags contain 5 wavy green bags, 5 plaid turquoise bags.\n" +
"bright chartreuse bags contain 2 muted gold bags, 5 posh olive bags, 1 vibrant tan bag, 2 faded salmon bags.\n" +
"dotted fuchsia bags contain 3 pale lime bags.\n" +
"pale teal bags contain 5 plaid silver bags, 4 shiny turquoise bags, 3 dim coral bags, 2 vibrant bronze bags.\n" +
"drab orange bags contain 2 shiny silver bags, 2 vibrant indigo bags, 5 mirrored lavender bags, 5 dark yellow bags.\n" +
"clear green bags contain 4 vibrant beige bags, 1 bright crimson bag, 5 muted gray bags.\n" +
"shiny yellow bags contain 4 light fuchsia bags, 2 mirrored plum bags.\n" +
"plaid blue bags contain 4 mirrored blue bags, 1 plaid olive bag.\n" +
"mirrored plum bags contain 2 dull red bags, 3 muted magenta bags.\n" +
"dim bronze bags contain 3 vibrant olive bags.\n" +
"posh green bags contain 3 mirrored blue bags, 4 vibrant yellow bags, 5 bright yellow bags, 2 wavy fuchsia bags.\n" +
"clear blue bags contain 5 mirrored yellow bags.\n" +
"dotted teal bags contain 5 shiny brown bags, 2 faded silver bags, 3 plaid coral bags.\n" +
"muted cyan bags contain 4 clear aqua bags.\n" +
"dark fuchsia bags contain 1 shiny blue bag, 1 dull fuchsia bag, 2 dull lime bags, 5 drab salmon bags.\n" +
"dark silver bags contain 2 dim salmon bags.\n" +
"muted beige bags contain 1 shiny lavender bag, 1 striped chartreuse bag.\n" +
"dull indigo bags contain 1 bright beige bag, 2 dark gold bags, 1 mirrored magenta bag.\n" +
"bright silver bags contain 3 faded violet bags, 4 dim cyan bags, 5 faded indigo bags, 1 clear chartreuse bag.\n" +
"dim olive bags contain 1 mirrored plum bag, 1 clear indigo bag, 2 striped crimson bags.\n" +
"pale magenta bags contain 5 vibrant black bags, 3 muted salmon bags, 1 dotted fuchsia bag.\n" +
"faded turquoise bags contain 2 light lavender bags, 3 clear lavender bags, 3 dull coral bags.\n" +
"posh gold bags contain 5 clear blue bags.\n" +
"muted maroon bags contain 5 dark fuchsia bags, 2 muted red bags, 5 dim salmon bags.\n" +
"muted tomato bags contain 4 striped blue bags, 3 dark plum bags, 5 faded green bags, 4 bright tan bags.\n" +
"dark lime bags contain 2 posh white bags, 4 dull crimson bags, 2 dark cyan bags, 4 mirrored plum bags.\n" +
"posh lime bags contain 1 wavy violet bag, 3 dotted teal bags, 5 shiny cyan bags.\n" +
"light tan bags contain 4 striped turquoise bags, 4 dim white bags, 4 shiny violet bags.\n" +
"dull bronze bags contain 2 drab teal bags, 5 light red bags, 4 muted indigo bags.\n" +
"plaid coral bags contain 5 dark purple bags, 5 striped crimson bags, 4 light beige bags.\n" +
"wavy lavender bags contain 5 muted white bags.\n" +
"drab indigo bags contain 4 dotted green bags, 1 striped silver bag.\n" +
"dull silver bags contain 1 dark yellow bag, 2 light beige bags, 3 striped indigo bags, 4 wavy violet bags.\n" +
"dark aqua bags contain 1 drab violet bag.\n" +
"muted bronze bags contain 4 clear chartreuse bags, 4 drab turquoise bags, 3 pale coral bags.\n" +
"striped coral bags contain 1 shiny red bag, 1 dark purple bag.\n" +
"faded beige bags contain 2 bright fuchsia bags, 2 mirrored turquoise bags.\n" +
"dull plum bags contain 5 dull aqua bags.\n" +
"drab lime bags contain 3 clear aqua bags, 4 shiny cyan bags.\n" +
"pale plum bags contain 5 shiny silver bags, 5 dull coral bags.\n" +
"pale violet bags contain 1 wavy purple bag, 1 light plum bag, 3 striped silver bags, 3 drab teal bags.\n" +
"clear magenta bags contain 2 dark plum bags, 2 striped tan bags, 4 posh plum bags, 4 dark turquoise bags.\n" +
"vibrant plum bags contain 3 dotted teal bags, 5 vibrant gold bags.\n" +
"bright gray bags contain 3 muted white bags, 2 striped crimson bags, 3 mirrored gray bags, 4 wavy silver bags.\n" +
"clear coral bags contain 2 vibrant magenta bags, 2 dim orange bags, 3 dark black bags.\n" +
"vibrant chartreuse bags contain 3 dark lime bags, 1 mirrored turquoise bag, 2 vibrant indigo bags, 1 dull gray bag.\n" +
"faded purple bags contain 2 mirrored indigo bags, 3 dark indigo bags, 2 pale fuchsia bags, 5 muted olive bags.\n" +
"drab violet bags contain 4 muted black bags, 1 clear maroon bag, 1 clear gold bag.\n" +
"dim magenta bags contain 5 wavy tan bags.\n" +
"clear orange bags contain 5 shiny tomato bags, 1 faded fuchsia bag.\n" +
"mirrored white bags contain 5 dim plum bags, 3 mirrored magenta bags.\n" +
"plaid white bags contain 1 pale tomato bag, 2 posh magenta bags, 5 vibrant maroon bags, 5 shiny fuchsia bags.\n" +
"shiny olive bags contain 4 striped salmon bags, 2 mirrored gray bags, 5 plaid beige bags.\n" +
"plaid plum bags contain 3 bright bronze bags.\n" +
"dull salmon bags contain 3 bright white bags, 2 muted cyan bags.\n" +
"pale maroon bags contain 3 muted tan bags, 1 bright gray bag.\n" +
"faded salmon bags contain 1 bright silver bag.\n" +
"striped magenta bags contain 5 dark plum bags, 5 faded bronze bags, 4 striped brown bags.\n" +
"vibrant crimson bags contain 2 muted lime bags, 5 plaid indigo bags.\n" +
"wavy tan bags contain 2 shiny yellow bags, 3 clear beige bags, 2 dotted gold bags.\n" +
"posh teal bags contain 4 striped gold bags, 3 drab salmon bags, 3 shiny plum bags, 2 pale gray bags.\n" +
"dotted coral bags contain 2 wavy chartreuse bags, 3 wavy blue bags.\n" +
"light purple bags contain 1 mirrored lime bag.\n" +
"bright lavender bags contain 5 faded brown bags.\n" +
"muted turquoise bags contain 2 shiny maroon bags, 3 light purple bags, 2 striped tan bags, 4 plaid cyan bags.\n" +
"posh red bags contain 2 striped lavender bags, 1 dark black bag, 5 dark purple bags, 3 drab olive bags.\n" +
"bright black bags contain 3 dark brown bags, 4 clear olive bags, 4 plaid olive bags.\n" +
"mirrored bronze bags contain 1 wavy turquoise bag.\n" +
"vibrant violet bags contain 2 plaid indigo bags, 4 faded orange bags.\n" +
"drab tan bags contain 3 clear crimson bags, 4 mirrored turquoise bags, 2 muted magenta bags.\n" +
"dim gray bags contain 5 striped black bags, 5 muted silver bags, 1 dotted blue bag, 3 posh gray bags.\n" +
"dull tan bags contain 1 clear maroon bag, 4 posh beige bags, 2 striped crimson bags, 5 dim gold bags.\n" +
"vibrant blue bags contain 4 bright beige bags, 5 bright bronze bags, 2 plaid salmon bags.\n" +
"posh crimson bags contain 2 mirrored purple bags, 1 striped silver bag, 2 bright magenta bags, 5 bright blue bags.\n" +
"dotted bronze bags contain 2 wavy crimson bags, 5 muted purple bags, 5 light gold bags.\n" +
"pale brown bags contain 1 drab coral bag, 2 mirrored lavender bags, 4 dark tomato bags, 3 plaid indigo bags.\n" +
"shiny maroon bags contain 5 light beige bags, 3 light white bags.\n" +
"dark tomato bags contain 5 shiny gold bags, 5 clear aqua bags, 2 dotted gold bags.\n" +
"dark beige bags contain 2 bright gray bags, 3 light beige bags, 1 dull lime bag.\n" +
"light red bags contain 5 mirrored magenta bags, 1 mirrored lime bag, 4 dotted teal bags.\n" +
"faded magenta bags contain 1 dim maroon bag, 3 mirrored gray bags, 1 dotted beige bag.\n" +
"drab turquoise bags contain no other bags.\n" +
"shiny salmon bags contain 5 shiny tan bags, 2 posh magenta bags, 4 clear turquoise bags.\n" +
"dark magenta bags contain 1 light fuchsia bag, 3 muted yellow bags, 5 dotted olive bags, 4 dark crimson bags.\n" +
"light gold bags contain 5 clear red bags.\n" +
"faded red bags contain 4 clear beige bags, 3 dim silver bags, 4 posh gray bags.\n" +
"pale salmon bags contain 5 mirrored white bags.\n" +
"striped lavender bags contain 2 striped blue bags.\n" +
"dull fuchsia bags contain 1 bright orange bag.\n" +
"dull turquoise bags contain 1 vibrant tomato bag, 3 clear tan bags, 3 bright teal bags, 1 drab maroon bag.\n" +
"dotted magenta bags contain 5 drab gold bags, 2 muted beige bags.\n" +
"pale indigo bags contain 4 plaid brown bags, 1 plaid cyan bag, 1 mirrored black bag, 5 drab magenta bags.\n" +
"drab yellow bags contain 5 dark turquoise bags, 2 dotted beige bags, 5 clear red bags, 2 muted lavender bags.\n" +
"striped gray bags contain 5 bright orange bags, 5 muted white bags, 2 clear chartreuse bags.\n" +
"dark lavender bags contain 5 dim gold bags, 2 pale coral bags.\n" +
"dull gold bags contain 1 dull beige bag.\n" +
"muted chartreuse bags contain 3 muted bronze bags, 1 faded black bag, 4 bright tan bags.\n" +
"plaid chartreuse bags contain 4 dim indigo bags, 4 mirrored gold bags, 4 dim lime bags.\n" +
"muted salmon bags contain 4 faded brown bags, 1 dotted black bag, 5 muted black bags.\n" +
"bright purple bags contain 5 shiny red bags, 5 vibrant cyan bags, 4 plaid cyan bags, 5 bright silver bags.\n" +
"dim chartreuse bags contain 1 faded indigo bag.\n" +
"drab purple bags contain 4 muted cyan bags, 3 wavy lavender bags, 2 dotted blue bags.";

}
